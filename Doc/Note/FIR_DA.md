# 基于分布式算法实现的FIR滤波器（DA）

## 1. 算法原理

DA在实现时，只包括查表、加法和减法运算。

DA的两个基本特征为：（1）将输入数据进行二进制分解；（2）将所有输入数据的相同位组合成一个单独数，并用该数计算或查表。

通用DA的输入输出关系如下：

<img src="E:\FPGA\Projects\2.Strengthen\SDRAM_RW\Doc\Note\pics\da_in_out.svg" style="zoom:100%;" />

DA的本质是将输入 $X_k$ 当作一个多位变量，并按照二进制分解表示如下：
$$
X_k=\sum\limits_{i=0}^{B-1}x_{k,i}\cdot2^i=\sum\limits_{i=0}^{B-1}x_k(i)\times 2^i
$$
其中，$x_{k,i}=x_k(i)$，是 $X_k$ 的第 `i` 位二进制表示。同时，将所有输入的第`i `位归并到一起计算，就是DA。

可以将DA理解为本来是一个一维向量计算$[X_0\quad\cdots\quad X_{N-1}]$，结果将$X_k$又拆分成B个一维向量，最后算法变成了$N\times B$维矩阵计算（该矩阵每列还有一个权重$2^i$的问题，但可以在并行计算时，采用转置单独提取出来）。
$$
\left[\matrix{
x_0(0)&\cdots & x_0{B-1} \\
\vdots & \ddots & \vdots \\
x_{N-1}(0) & \cdots & x_{N-1}(B-1)
}\right]
$$
分解后，由于矩阵只包含0和1变量，变换后乘法不存在了。因为矩阵可以进行并行运算，所以有并行计算的优势。

> DA的实现原理

DA的推导过程如下：

- 将滤波器表示为：$\begin{aligned}y=\sum\limits_{k=0}^{N-1}A_kX_k \end{aligned}$，其中$A_k$是滤波系数，$X_k$是输入变量，`N`是内积的次数即滤波器Tap长度。

- 将无符号的$X_k$用二进制数表示：$\begin{aligned}X_k=\sum\limits_{i=0}^{B-1}x_{k,i}\cdot2^i \end{aligned}$，其中 $x_{k,i}$ 是 $X_k$ 的第 `i` 位二进制数（0或1），而`B`为 $X_k$ 的总位数。滤波器内积计算结果为：
  $$
  y=\sum\limits_{k=0}^{N-1}A_kX_k
   =\sum\limits_{k=0}^{N-1}A_k\sum\limits_{i=0}^{B-1}x_{k,i}\times 2^i
   =\sum\limits_{i=0}^{B-1}2^i\sum\limits_{k=0}^{N-1}A_k\cdot x_{k,i}
  $$
  对于二进制补码表示的有符号数$X_k$（可以表示为$\begin{aligned}X_k=-2^{B-1}\cdot x_{k,B-1}+\sum\limits_{i=0}^{B-2}2^i\cdot x_{k,i} \end{aligned}$），则可以将上式改写为：
  $$
  y=\sum\limits_{k=0}^{N-1}A_k(-2^{B-1}x_{k,B-1}+\sum\limits_{i=0}^{B-2}2^i\cdot x_{k,i}\ )
   =-2^{B-1}\sum\limits_{k=0}^{N-1}A_kx_{k,B-1}+\sum\limits_{i=0}^{B-2}(2^i\sum\limits_{k=0}^{N-1}A_kx_{k,i})
  $$

- 计算$\begin{aligned}f_{B-1}=\sum\limits_{k=0}^{N-1}A_kx_{k,B-1} \end{aligned}$，以及$\begin{aligned}f_i=\sum\limits_{k=0}^{N-1}A_kx_{k,i},\forall i=0,1,\cdots,B-2\end{aligned}$.

- 最终的滤波结果可变换为：$\begin{aligned}y=-2^{B-1}f_{B-1}+\sum\limits_{i=0}^{B-2}2^if_i \end{aligned}$.

以上过程就是DA的核心实现思想。

如果将$f_i$展开，可得：$f_i=A_0x_{0,i}+A_1x_{1,i}+A_2x_{2,i}+\cdots+A_{N-1}x_{N-1,i}$

同理将$f_{B-1}$展开可得：$f_{B-1}=A_0x_{0,B-1}+A_1x_{1,B-1}+A_2x_{2,B-1}+\cdots+A_{N-1}x_{N-1,B-1}$

以上两式有不同的表达式含义，其中，$f_{B-1}$是对符号位进行操作，而$f_i$是对普通二进制数据进行操作。

由于$x_{k,i}$为二进制数0或1，而FIR滤波器的系数$A_k$为常数，因此$f_i$的结果可以通过一个常数查找表得出。

由$f_i=A_0x_{0,i}+A_1x_{1,i}+A_2x_{2,i}+\cdots+A_{N-1}x_{N-1,i}$可知，DA的查找表内容如下所示：

| 输入（$x_{N-1,i}\quad x_{N-2,i} \quad \cdots \quad x_{0,i} $） |           输出$f_i$           |
| :----------------------------------------------------------: | :---------------------------: |
|                         $00\cdots0$                          |               0               |
|                         $00\cdots1$                          |             $A_0$             |
|                         $00\cdots10$                         |             $A_1$             |
|                         $00\cdots11$                         |           $A_0+A_1$           |
|                           $\cdots$                           |           $\cdots$            |
|                         $11\cdots1$                          | $A_0+A_1+A_2+\cdots +A_{N-1}$ |

【个人理解】在实际计算时，缓存一组长度等于滤波器长度的数据，根据这些每个数据同权值的比特所组成的新数查表得到结果，根据结果再进行加法计算即可得到该组数据的滤波结果。上表在滤波器系数已知后，即为$2^N$个查找表。

基于DA实现的通用电路结构如下图所示：

<img src="pics/FIR_DA_Circuit.svg" style="zoom:120%;" />

## 2. HDL实现

HDL实现暂时没有考虑滤波数据输出时的幅度调整。

```verilog
module fir_da 
#(
    parameter   N_taps    = 9,
    parameter   BIT_WIDTH = 16
)(
    input   wire    clk,
    input   wire    clk_en,
    input   wire    rst_n,
    input   wire    signed  [BIT_WIDTH-1:0] filter_in,
    output  reg     signed  [BIT_WIDTH-1:0] filter_out
);
    localparam  DA_WIDTH = BIT_WIDTH  + `log2(N_taps);
    localparam  SUM_WIDTH = BIT_WIDTH + `log2(BIT_WIDTH) + `log2(N_taps);

    //定义一个存储器，用来缓存输入的数据
    reg     signed  [N_taps-1:0]    delay_pipeline  [BIT_WIDTH-1:0];

    wire    signed  [DA_WIDTH-1:0]  DA_data [BIT_WIDTH-1:0];
    wire    signed  [SUM_WIDTH-1:0] sum     [BIT_WIDTH:0];

    assign  sum[0] = 0;

    integer j;
    //该生成块用于实现存储器的初始化，并将输入数据存储到比特矩阵中
    //如果for循环的方式不利于理解，可以将其展开看
    generate
        genvar i;
        for(i=0;i < BIT_WIDTH;i=i+1)begin:BIT_WIDTH_1bit_LUT
            always@(posedge clk)begin 
                if(rst_n == 1'b0)begin 
                    for(j=0;j < N_taps;j=j+1)begin:bit_matrix_intial 
                        delay_pipeline[i][j] <= 1'b0;           //初始化比特矩阵
                    end
                end
                else begin 
                    if(clk_en == 1'b1)begin 
                        delay_pipeline[i][0] <= filter_in[i];
                        for(j=1;j < N_taps;j=j+1)begin:bit_matrix
                            delay_pipeline[i][j] <= delay_pipeline[i][j-1];
                        end
                    end
                end
            end
            //通过DA查找表，得到每个位的计算结果
            DA_ROM #(N_taps,DA_WIDTH) U_bit(.addr(delay_pipeline[i]), .data(DA_data[i]));
            //对每位结果进行移位累加，保证权重正确
            assign  sum[i+1] = sum[i] + (DA_data[i] << i);
        end
    endgenerate

    //输出滤波器结果，没有做幅度调整
    always@(posedge clk)begin 
        if(rst_n == 1'b0)
            filter_out <= 0;
        else if(clk_en == 1'b1)     
            filter_out <= sum[N_taps][SUM_WIDTH - 1 : SUM_WIDTH - BIT_WIDTH];
        else
            filter_out <= filter_out;
    end
endmodule
```

```verilog
//给出N阶滤波器，N个位组成的查找表。所有位数都使用相同的表
module DA_ROM 
#(
    parameter   N_taps = 5,
    parameter   ROM_WIDTH = 18
)(
    input       [N_taps-1:0]    addr,
    input   reg [ROM_WIDTH-1:0] data
);
    always@(addr)begin 
        case (addr)
            5'b00000 : data = 18'b00_0000_0000_0000_0000; 
            5'b00001 : data = 18'b00_0011_0000_1010_0000;
            5'b11111 : data = 18'b01_1001_1101_0101_0000;
            default  : data = 18'b01_1001_1101_0101_0000;
        endcase
    end
endmodule
```



## 3. DA的改进

在不考虑二进制权重时，DA可以表示为如下运算：
$$
y = \left[\matrix{A(0) & A(1) & \cdots & A(N-1)} \right]
\times
\left[\matrix{x_0(0) & \cdots & x_0(B-1)
\\
\vdots & \ddots & \vdots
\\
x_{N-1}(0) & \cdots & x_{N-1}(B-1)
}
\right]
$$
对单个行列式$y(i)$做如下拆解：
$$
\begin{aligned}
y(i)&=\left[\matrix{A(0) & \cdots & A(N-1)}\right]
\times
\left[\matrix{x_0(i) & \cdots & x_{N-1}(i)}\right]
\\
\\
&={\rm sum}
\left(\matrix{[A(0)\quad \cdots \quad A(3)]*[x_0(i)\quad \cdots x_3(i)]+\cdots+\\ 
[A(k-4)\quad \cdots \quad A[k-1]*[x_{k-4}(i)\quad\cdots\quad x_{k-1}(i)]+\cdots+ \\
[A(N-4)\quad\cdots\quad A(N-1)]*[x_{N-4}(i)\quad\cdots\quad x_{N-1}(i)] }
\right)\\
\\
&={\rm trace}\left(\matrix{
\left[\matrix{
A(0) & \cdots & A(3)\\
\vdots & \ddots & \vdots \\
A(N-4) & \cdots & A(N-1)
}
\right]*
\left[\matrix{
x_0(i) & \cdots & x_{N-4}(i) \\
\vdots & \ddots & \vdots \\
x_3(i) & \cdots & x_{N-1}(i)
}
\right]
}\right)
\end{aligned}
$$
上述变换等效于将1个$2^N$项的查找表转换为$N/4$个$2^4$项查找表，因为针对$[x_0(i)\quad\cdots\quad x_3(i)]$，只需要查找$[A(0)\quad\cdots\quad A(3)]$的组合表，而对$[x_{k-4}(i)\quad\cdots\quad x_{k-1}(i)]$，只需要查找$[A(k-4)\quad\cdots\quad A(k-1)]$的组合表。

如果FPGA为6输入查找表，可以将这个化简为$N/6$个$2^6$项查找表。

令$f_{i,j}=A(4j)\times x_{4j}(i)+A(4j+1)\times x_{4j+1}(i)+A(4j+2)\times x_{4j+2}(i) + A(4j+3)\times x_{4j+3}(i)$，

则上述过程可以梳理为：
$$
y=-2^{B-1}\sum\limits_{j=0}^{N/4-1}f_{B-1,j}+\sum\limits_{i=0}^{B-2}2^i\sum\limits_{j=0}^{N/4-1}f_{i,j}
=\sum\limits_{j=0}^{N/4-1}\left(
-2^{B-1}f_{B-1,j}+\sum\limits_{i=0}^{B-2}2^i f_{i,j}
\right)
$$
令$\begin{align}y_i=-2^{B-1}f_{B-1,j}+\sum\limits_{i=0}^{B-2}2^i f_{i,j}\end{align}$，则$y_i$与标准DA完全一致，等效于计算一个4输入的标准DA。计算出$y_i$后，可以采用串行或一次性并行累加出最终的结果$y$。

优化后的DA实现如图所示。

![优化后的DA实现](E:\FPGA\Projects\2.Strengthen\SDRAM_RW\Doc\Note\pics\da_opt.svg)