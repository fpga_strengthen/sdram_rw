module  fifo_ctrl(
    //时钟和复位
    input   wire        i_clk_100M,                 //100M时钟
    input   wire        i_rst_n,                    //复位，低有效
    //SDRAM初始化
    input   wire        i_init_end,                 //初始化完成信号，完成后常高
    //写FIFO信号
    input   wire        i_wr_fifo_wr_clk,           //FIFO写时钟，50M，将串口数据写入FIFO
    input   wire        i_wr_fifo_wr_req,           //FIFO写请求，交给仲裁模块以得到ack信号
    input   wire [15:0] iv_wr_fifo_wr_data,         //写FIFO写入的数据
    input   wire [9:0]  iv_wr_burst_len,            //写突发长度
    input   wire [23:0] iv_sdram_wr_baddr,          //SDRAM写操作的起始地址，baddr~begin_addr
    input   wire [23:0] iv_sdram_wr_eaddr,          //SDRAM写操作的结束地址，eaddr~end_addr
    input   wire        i_wr_rst,                   //写复位信号，对系统复位取反

    //读FIFO信号
    input   wire        i_rd_fifo_rd_clk,           //FIFO读时钟，将数据读出后发送给串口
    input   wire        i_rd_fifo_rd_req,           //FIFO读请求，交给仲裁模块以得到ack信号
    input   wire        i_read_valid,               //读有效信号
    input   wire [9:0]  iv_rd_burst_len,            //读操作的突发长度
    input   wire [23:0] iv_sdram_rd_baddr,          //SDRAM读操作的起始地址，baddr~begin_addr
    input   wire [23:0] iv_sdram_rd_eaddr,          //SDRAM读操作的结束地址，eaddr~end_addr
    input   wire        i_rd_rst,                   //读操作复位信号
    output  wire [15:0] ov_rd_fifo_data,            //从FIFO读出数据，时钟50M，发送给串口
    output  wire [9:0]  ov_rd_fifo_num,             //读FIFO时FIFO中的数据个数

    //SDRAM写信号
    input   wire        i_wr_ack,                   //写ack信号，与SDRAM_write写入的数据基本同步（会比数据早一拍）
    output  reg         o_sdram_wr_req,             //SDRAM写请求信号，交给仲裁模块
    output  wire [15:0] ov_sdram_data_in,           //写入SDRAM的数据
    output  reg  [23:0] ov_sdram_wr_addr,           //SDRAM写数据的地址，只需要首地址

    //SDRAM读信号
    input   wire        i_rd_ack,                   //读ack信号，与SDRAM_read回读的数据完全同步
    input   wire [15:0] iv_rd_sdram_out,            //从SDRAM读出的数据
    output  reg         o_sdram_rd_req,             //SDRAM读请求信号，交给仲裁模块
    output  reg  [23:0] ov_sdram_rd_addr            //SDRAM读数据的地址，只需要首地址
);

//------------------------------------------ 写FIFO -------------------------------------------//
    //reg & wire define
    wire    [9:0]   wr_fifo_num;                    //对写入FIFO的数据计数
    wire    [9:0]   rd_fifo_num;                    //写FIFO操作下从FIFO读出的数据

    //提取ack下降沿作为写结束标志，切换地址
    reg     wr_ack_reg;
    reg     rd_ack_reg;
    
    wire    wr_ack_n;
    wire    rd_ack_n;

    //注：wrusedw表示FIFO中已经写入了多少数据，rdusedw表示FIFO中还有多少数据可供读取
    //写FIFO，该实例将50M的串口数据写入FIFO，按照100M读出写入SDRAM
    fifo_data_rw U_wr_fifo(
    	//用户接口
    	.wrclk  (i_wr_fifo_wr_clk),
    	.wrreq  (i_wr_fifo_wr_req),
    	.data   (iv_wr_fifo_wr_data),
        //SDRAM接口
    	.rdclk  (i_clk_100M),
    	.rdreq  (i_wr_ack),
    	.q      (ov_sdram_data_in),
        //清零与数据量接口
        .aclr   (i_wr_rst || (~i_rst_n)),
        .rdusedw(wr_fifo_num),                   //表示FIFO中当前可供读取的数据量
    	.wrusedw()                                  //悬空，无需连接
    );

    //读FIFO，该实例将从SDRAM读出的数据按照100M时钟写入FIFO，按50M读出后发送到串口
    fifo_data_rw U_rd_fifo(
    	//SDRAM接口
    	.wrclk  (i_clk_100M),
    	.wrreq  (i_rd_ack),
    	.data   (iv_rd_sdram_out),
        //用户接口
    	.rdclk  (i_rd_fifo_rd_clk),
    	.rdreq  (i_rd_fifo_rd_req),
    	.q      (ov_rd_fifo_data),
        //清零与数据量接口
        .aclr   (i_rd_rst || (~i_rst_n)),               //该端口复位信号高有效
        .rdusedw(),
    	.wrusedw(rd_fifo_num)                        //当FIFO已写入的数据量等于突发长度且读有效时，表示有数据可以读，这样就不会空读
    );

    assign  ov_rd_fifo_num = rd_fifo_num;

    //提取读/写操作的ack信号下降沿
    always@(posedge i_clk_100M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            wr_ack_reg <= 1'b0;
        else
            wr_ack_reg <= i_wr_ack;
    end

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_ack_reg <= 1'b0;
        else
            rd_ack_reg <= i_rd_ack;
    end

    //下降沿，表示一次突发操作结束
    assign  wr_ack_n = (~i_wr_ack) & (wr_ack_reg);
    assign  rd_ack_n = (~i_rd_ack) & (rd_ack_reg); 


    //读地址更新（只需要更新首地址）
    //根据开始地址和结束地址，如果每次突发写操作结束后写入的数据量仍然少于目标地址数，则每次突发结束后更新写地址需要加上突发长度
    //当写入地址数足够时，则重置初始地址
    always@(posedge i_clk_100M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            ov_sdram_wr_addr <= 24'b0;
        else if(i_wr_rst == 1'b1)           //写复位，高有效
            ov_sdram_wr_addr <= iv_sdram_wr_baddr;
        else if(wr_ack_n == 1'b1)begin 
            if(ov_sdram_wr_addr < (iv_sdram_wr_eaddr - iv_wr_burst_len))
                ov_sdram_wr_addr <= ov_sdram_wr_addr + iv_wr_burst_len;
            else
                ov_sdram_wr_addr <= iv_sdram_wr_baddr;
        end
        else
            ov_sdram_wr_addr <= ov_sdram_wr_addr;
    end

    //更新读地址，与更新写地址类似
    always@(posedge i_clk_100M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            ov_sdram_rd_addr <= 24'b0;
        else if(i_wr_rst == 1'b1)           //写复位，高有效
            ov_sdram_rd_addr <= iv_sdram_rd_baddr;
        else if(rd_ack_n == 1'b1)begin
            if(ov_sdram_rd_addr < (iv_sdram_rd_eaddr - iv_rd_burst_len))
                ov_sdram_rd_addr <= ov_sdram_rd_addr + iv_rd_burst_len;
            else
                ov_sdram_rd_addr <= iv_sdram_rd_baddr;
        end
        else
            ov_sdram_rd_addr <= ov_sdram_rd_addr;
    end

    //读/写请求信号
    always@(posedge i_clk_100M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin 
            o_sdram_wr_req <= 1'b0;
            o_sdram_rd_req <= 1'b0;
        end
        else if(i_init_end == 1'b1)begin 
            if(wr_fifo_num >= iv_wr_burst_len)begin
                o_sdram_wr_req <= 1'b1;         //写FIFO中数据量满足一次突发长度时就将数据写入SDRAM
                o_sdram_rd_req <= 1'b0;
            end
            else if((rd_fifo_num < iv_rd_burst_len)&&(i_read_valid == 1'b1))begin 
                o_sdram_wr_req <= 1'b0;         //读FIFO中数据量满足一次突发长度时就将数据读出SDRAM
                o_sdram_rd_req <= 1'b1;               
            end
            else begin
                o_sdram_wr_req <= 1'b0;
                o_sdram_rd_req <= 1'b0;
            end
        end
        else begin
            o_sdram_wr_req <= 1'b0;
            o_sdram_rd_req <= 1'b0; 
        end
    end

endmodule