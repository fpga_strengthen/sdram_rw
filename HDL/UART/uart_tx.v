//uart并串转换
//波特率：9600bps

module uart_tx
#(
    parameter UART_BPS = 'd9600,
    parameter CLK_FREQ = 'd50_000_000
)
(
    sys_clk,
    sys_rst_n,
    iv_uart_byte,
    i_uart_byte_sck,
    o_data_bit
);
    input   wire        sys_clk;
    input   wire        sys_rst_n;
    input   wire [7:0]  iv_uart_byte;
    input   wire        i_uart_byte_sck;

    output  reg         o_data_bit;

    localparam  CNT_BAUD_MAX = CLK_FREQ / UART_BPS;

    reg         work_en;        //控制每个bit持续时间的计数器的使能信号
    reg         bit_flag;       //在每个比特中间时刻采样输出
    reg [12:0]  cnt_baud;       //控制每个bit持续时间的计数器，对应9600bps
    reg [3:0]   cnt_bit;        //对输出的bit计数，计一帧，10bit

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            work_en <= 1'b0;
        else if(i_uart_byte_sck == 1'b1)        //字节有效信号拉高时，进入并串转换状态
            work_en <= 1'b1;
        else if((cnt_bit == 4'd9)&&(bit_flag == 1'b1))               //读取出最后一个bit时，将工作状态拉低，发送完毕
            work_en <= 1'b0;
        else  
            work_en <= work_en;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            cnt_baud <= 13'd0;
        else if(cnt_baud == CNT_BAUD_MAX)       //一个bit的持续时间
            cnt_baud <= 13'd0;
        else if((cnt_bit == 4'd9)&&(bit_flag == 1'b1))               //最后一个停止位
            cnt_baud <= 13'd0;
        else if(work_en == 1'b1)                
            cnt_baud <= cnt_baud + 13'd1;
        else
            cnt_baud <= 13'd0;
    end

    //在每个bit的中间时刻拉高比特有效信号，保证信号的稳定
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            bit_flag <= 1'b0;
        else if(cnt_baud == (CNT_BAUD_MAX / 2))
            bit_flag <= 1'b1;
        else
            bit_flag <= 1'b0;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_bit <= 4'd0;
        else if((cnt_bit == 4'd9)&&(bit_flag == 1'b1))      //第10个比特为停止位，此时可以不计，待cnt_bit=9时且bit_flag有效时直接发送停止位即可
            cnt_bit <= 4'd0;
        else if(bit_flag == 1'b1)
            cnt_bit <= cnt_bit + 4'd1;
        else
            cnt_bit <= cnt_bit;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            o_data_bit <= 1'b1;         //空闲状态为高
        else if(bit_flag == 1'b1)       //注意是在bit_flag拉高的前提下才能发送比特，所以不用担心cnt_bit=0时会持续发送起始位
            case(cnt_bit)
                4'd0    : o_data_bit <= 1'b0;                 //发送起始位
                4'd1    : o_data_bit <= iv_uart_byte[0];      //发送第一个比特，按照从低到高的顺序发送
                4'd2    : o_data_bit <= iv_uart_byte[1];      //发送第二个bit
                4'd3    : o_data_bit <= iv_uart_byte[2];      //发送第三个bit
                4'd4    : o_data_bit <= iv_uart_byte[3];      //发送第四个bit
                4'd5    : o_data_bit <= iv_uart_byte[4];      //发送第五个bit
                4'd6    : o_data_bit <= iv_uart_byte[5];      //发送第六个bit
                4'd7    : o_data_bit <= iv_uart_byte[6];      //发送第七个bit
                4'd8    : o_data_bit <= iv_uart_byte[7];      //发送第八个bit      
                4'd9    : o_data_bit <= 1'b1;                 //发送停止位bit           
                default : o_data_bit <= 1'b1;                 //非正常情况下认为是空闲状态
            endcase
        else
            o_data_bit <= o_data_bit;
    end


endmodule