//uart接收模块。进行串并转换
//波特率：9600bps
//串行输入比特的帧结构：1起始位(低电平)+8bit数据+1停止位(高电平)，空闲时为高

module uart_rx
#(
    parameter   UART_BPS = 'd9600,              //串口波特率
    parameter   CLK_FREQ = 'd50_000_000         //时钟频率，50M
)
(
    sys_clk,
    sys_rst_n,
    rx,
    ov_uart_byte,
    o_uart_byte_sck
);
    input   wire      sys_clk;              //系统时钟,50M
    input   wire      sys_rst_n;            //系统复位，低有效
    input   wire      rx;                   //串行输入bit

    output  reg [7:0] ov_uart_byte;         //串并转换输出字节
    output  reg       o_uart_byte_sck;      //与字节同步的sck信号

    localparam  CNT_BAUD_MAX = CLK_FREQ / UART_BPS;

    //对串行输入比特打拍，取起始位的跳变沿作为flag信号开始接收
    reg     rx_r1,rx_r2,rx_r3;  
    reg     rx_flag;                    //rx的下降沿作为标志信号启动接收

    //在每一个bit稳定时对该比特进行采样，在中间时刻采样(只要不在bit变化时采样得到的比特数据就是稳定的)
    reg         bit_sample_flag;        //采样脉冲标志信号
    reg [12:0]  cnt_baud;               //波特计数器，每计数到5208会有一个比特的变化;当接收状态即work_en拉高时进行计数
    //如果数据有下降沿的变化，则可能会造成rx_flag的误判；为了区别，引出work_en信号来作为接收与否的使能信号
    reg         work_en;                //接收使能信号，接收时为高，接收完后为低
    reg [7:0]   uart_byte_temp;
    reg         rx_end_sck;
    
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)begin
            rx_r1 <= 1'b1;      //空闲状态为高，所以复位拉高
            rx_r2 <= 1'b1;
            rx_r3 <= 1'b1;
        end
        else begin 
            rx_r1 <= rx;
            rx_r2 <= rx_r1;
            rx_r3 <= rx_r2;
        end
    end

    //开始接收串行数据的标志信号
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            rx_flag <= 1'b0;
        else if((rx_r2 == 1'b0)&&(rx_r3 == 1'b1))
            rx_flag <= 1'b1;
        else
            rx_flag <= 1'b0;
    end

    //波特率为9600bps，即每秒9600比特，每个比特的时间为1/9600s;
    //时钟周期为50M,时间为1/(50M),则每个比特对应的时钟周期数为(1/9600)/(1/(50M))≈5208，小数部分的误差可以暂时忽略。
    //因为每个比特的时钟周期数很多，因此细小的小数部分差异对于比特的接收影响很小
    
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_baud <= 13'd0;
        else if((cnt_baud == CNT_BAUD_MAX - 1'b1)||(work_en == 1'b0))       //注意在work_en为低时该计数器是不计数的
            cnt_baud <= 13'd0;
        else if(work_en == 1'b1)
            cnt_baud <= cnt_baud + 13'd1;
        else
            cnt_baud <= cnt_baud;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            bit_sample_flag <= 1'b0;
        else if(cnt_baud == (CNT_BAUD_MAX /2 ))          //CNT_BAUD_MAX/2 - 1
            bit_sample_flag <= 1'b1;
        else
            bit_sample_flag <= 1'b0;
    end
    
    //在采样时刻对比特进行计数,计入的第0个bit是起始位，第1~8个为数据，第9个为停止位
    //由于计到8时这一帧中的数据已经全部得到，所以是否计到9无关紧要
    reg     [3:0]   cnt_bit;            //串行输入比特计数器

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_bit <= 4'd0;
        else if(cnt_bit == 4'd9)
            cnt_bit <= 4'd0;
        else if(bit_sample_flag == 1'b1)
            cnt_bit <= cnt_bit + 4'b1;
        else
            cnt_bit <= cnt_bit;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            work_en <= 1'b0;
        else if((rx_flag == 1'b1)&&(work_en == 1'b0))
            work_en <= 1'b1;
        else if((cnt_bit == 4'd8)&&(bit_sample_flag == 1'b1))       //当第8个数据比特被完全采样后才能将owrk_en拉低
            work_en <= 1'b0;
        else
            work_en <= work_en;
    end
    //每当采样时刻将采样的数据移入寄存器暂存；当8位全部收到后再输出
    //串口助手发送的顺序是先发低位，再发高位；因此uart每次接收的比特应该放在高位，并每接收到1bit就向右移位一次

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            uart_byte_temp <= 8'b0;
        else if((cnt_bit >= 4'd1)&&(cnt_bit <= 4'd8)&&(bit_sample_flag == 1'b1))
            uart_byte_temp <= {rx_r3,uart_byte_temp[7:1]};      //移入的应当是打拍后的数据
        else 
            uart_byte_temp <= uart_byte_temp;
    end

    //当接收完8bit时发出一个接收完成的sck信号
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            rx_end_sck <= 1'b0;
        else if((cnt_bit == 4'd8)&&(bit_sample_flag == 1'b1))
            rx_end_sck <= 1'b1;
        else
            rx_end_sck <= 1'b0;
    end

    //输出串并转换后的数据和同步的sck信号
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            ov_uart_byte <= 8'b0;
        else if(rx_end_sck == 1'b1)
            ov_uart_byte <= uart_byte_temp;         //仅在接收完8bit后才输出串并转换的结果
        else
            ov_uart_byte <= ov_uart_byte;
    end

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            o_uart_byte_sck <= 1'b0;
        else begin 
            o_uart_byte_sck <= rx_end_sck;          //将接收完成标志信号打一拍输出，与输出字节同步
        end
    end

endmodule