module sdram_top(
    //时钟和复位
    input   wire        i_clk_100M,                 //100M时钟
    input   wire        i_rst_n,                    //复位，低有效
    input   wire        i_clk_out,                  //针对相移

    //写FIFO信号
    input   wire        i_wr_fifo_wr_clk,           //FIFO写时钟，50M，将串口数据写入FIFO
    input   wire        i_wr_fifo_wr_req,           //FIFO写请求，交给仲裁模块以得到ack信号
    input   wire [15:0] iv_wr_fifo_wr_data,         //写FIFO写入的数据
    input   wire [9:0]  iv_wr_burst_len,            //写突发长度
    input   wire [23:0] iv_sdram_wr_baddr,          //SDRAM写操作的起始地址，baddr~begin_addr
    input   wire [23:0] iv_sdram_wr_eaddr,          //SDRAM写操作的结束地址，eaddr~end_addr
    input   wire        i_wr_rst,                   //写复位信号，对系统复位取反

    //读FIFO信号
    input   wire        i_rd_fifo_rd_clk,           //FIFO读时钟，将数据读出后发送给串口
    input   wire        i_rd_fifo_rd_req,           //FIFO读请求，交给仲裁模块以得到ack信号
    input   wire        i_read_valid,               //读有效信号
    input   wire [9:0]  iv_rd_burst_len,            //读操作的突发长度
    input   wire [23:0] iv_sdram_rd_baddr,          //SDRAM读操作的起始地址，baddr~begin_addr
    input   wire [23:0] iv_sdram_rd_eaddr,          //SDRAM读操作的结束地址，eaddr~end_addr
    input   wire        i_rd_rst,                   //读操作复位信号
    
    output  wire [15:0] ov_rd_fifo_data,            //从FIFO读出数据，时钟50M
    output  wire [9:0]  ov_rd_fifo_num,             //读FIFO时FIFO中的数据个数

    output  wire        o_sdram_clk,
    output  wire        o_sdram_clk_cke,
    output  wire        o_sdram_cs_n,
    output  wire        o_sdram_ras_n,
    output  wire        o_sdram_cas_n,
    output  wire        o_sdram_we_n,
    output  wire [1:0]  ov_sdram_bank,
    output  wire [12:0] ov_sdram_addr,
    output  wire [1:0]  ov_sdram_dqm,               //掩码
    inout   wire [15:0] io_sdram_dq                 //输入输出端口
);

assign      o_sdram_clk  = i_clk_out;
assign      ov_sdram_dqm = 2'b00;

//------------------------wire define-----------------------

    wire        init_end;                  //SDRAM初始化完成信号，完成后常高

    //SDRAM写信号
    wire        wr_ack;                    //写ack信号，与SDRAM_write写入的数据基本同步（会比数据早一拍）
    wire        sdram_wr_req;              //SDRAM写请求信号，交给仲裁模块
    wire [15:0] sdram_data_in;             //写入SDRAM的数据
    wire [23:0] sdram_wr_addr;             //SDRAM写数据的地址，只需要首地址

    //SDRAM读信号
    wire        rd_ack;                    //读ack信号，与SDRAM_read回读的数据完全同步
    wire [15:0] rd_sdram_out;              //从SDRAM读出的数据
    wire        sdram_rd_req;              //SDRAM读请求信号，交给仲裁模块
    wire [23:0] sdram_rd_addr;             //SDRAM读数据的地址，只需要首地址


//----------------------FIFO控制实例化调用-------------------------
    fifo_ctrl U_fifo_ctrl(
        //时钟和复位
        .i_clk_100M         (i_clk_100M),                   //100M时钟
        .i_rst_n            (i_rst_n   ),                   //复位，低有效
        //SDRAM初始化
        .i_init_end         (init_end),                     //初始化完成信号，完成后常高
        //写FIFO信号
        .i_wr_fifo_wr_clk   (i_wr_fifo_wr_clk  ),           //FIFO写时钟，50M，将串口数据写入FIFO
        .i_wr_fifo_wr_req   (i_wr_fifo_wr_req  ),           //FIFO写请求，交给仲裁模块以得到ack信号
        .iv_wr_fifo_wr_data (iv_wr_fifo_wr_data),           //写FIFO写入的数据
        .iv_wr_burst_len    (iv_wr_burst_len   ),           //写突发长度
        .iv_sdram_wr_baddr  (iv_sdram_wr_baddr ),           //SDRAM写操作的起始地址，baddr~begin_addr
        .iv_sdram_wr_eaddr  (iv_sdram_wr_eaddr ),           //SDRAM写操作的结束地址，eaddr~end_addr
        .i_wr_rst           (i_wr_rst          ),           //写复位信号，对系统复位取反

        //读FIFO信号
        .i_rd_fifo_rd_clk   (i_rd_fifo_rd_clk ),            //FIFO读时钟，将数据读出后发送给串口
        .i_rd_fifo_rd_req   (i_rd_fifo_rd_req ),            //FIFO读请求，交给仲裁模块以得到ack信号
        .i_read_valid       (i_read_valid     ),            //读有效信号
        .iv_rd_burst_len    (iv_rd_burst_len  ),            //读操作的突发长度
        .iv_sdram_rd_baddr  (iv_sdram_rd_baddr),            //SDRAM读操作的起始地址，baddr~begin_addr
        .iv_sdram_rd_eaddr  (iv_sdram_rd_eaddr),            //SDRAM读操作的结束地址，eaddr~end_addr
        .i_rd_rst           (i_rd_rst         ),            //读操作复位信号
        .ov_rd_fifo_data    (ov_rd_fifo_data  ),            //从FIFO读出数据，时钟50M，读出后交给FIFO读模块缓存后按照串口时序输出给Uart_tx模块
        .ov_rd_fifo_num     (ov_rd_fifo_num   ),            //读FIFO时FIFO中的数据个数

        //SDRAM写信号
        .i_wr_ack           (wr_ack        ),               //写ack信号，与SDRAM_write写入的数据基本同步（会比数据早一拍）
        .o_sdram_wr_req     (sdram_wr_req  ),               //SDRAM写请求信号，交给仲裁模块
        .ov_sdram_data_in   (sdram_data_in ),               //写入SDRAM的数据
        .ov_sdram_wr_addr   (sdram_wr_addr ),               //SDRAM写数据的地址，只需要首地址

        //SDRAM读信号
        .i_rd_ack           (rd_ack       ),                //读ack信号，与SDRAM_read回读的数据完全同步
        .iv_rd_sdram_out    (rd_sdram_out ),                //从SDRAM读出的数据
        .o_sdram_rd_req     (sdram_rd_req ),                //SDRAM读请求信号，交给仲裁模块
        .ov_sdram_rd_addr   (sdram_rd_addr)                 //SDRAM读数据的地址，只需要首地址
    );

//----------------------SDRAM_Ctrl-------------------------
    sdram_ctrl U_sdram_ctrl(
        //时钟和复位信号
        .i_clk_100M     (i_clk_100M),
        .i_rst_n        (i_rst_n   ), 

        //SDRAM写端口
        .i_wr_req       (sdram_wr_req   ),                  //FIFO控制模块提供
        .iv_wr_addr     (sdram_wr_addr  ),
        .iv_wr_data     (sdram_data_in  ),
        .iv_wr_burst_len(iv_wr_burst_len),
        .o_wr_ack       (wr_ack         ),                  //与写数据同步的ack信号

        //SDRAM读端口
        .i_rd_req       (sdram_rd_req   ),                  //FIFO控制模块提供
        .iv_rd_addr     (sdram_rd_addr  ),
        .iv_rd_burst_len(iv_rd_burst_len),
        .ov_rd_data_out (rd_sdram_out   ),                  //读出的数据到FIFO跨时钟域处理
        .o_rd_ack       (rd_ack         ),

        //SDRAM硬件接口
        .o_sdram_clk_cke(o_sdram_clk_cke),
        .o_sdram_cs_n   (o_sdram_cs_n   ),
        .o_sdram_ras_n  (o_sdram_ras_n  ),
        .o_sdram_cas_n  (o_sdram_cas_n  ),
        .o_sdram_we_n   (o_sdram_we_n   ),
        .ov_sdram_bank  (ov_sdram_bank  ),
        .ov_sdram_addr  (ov_sdram_addr  ),

        //初始化完成信号
        .o_init_end     (init_end),

        //接收和发送数据的端口
        .io_sdram_dq    (io_sdram_dq)                        //与顶层的数据输入输出端口连接
);
    
endmodule