module clk_gen(
    i_clk_50M,
    i_rst_n,
    o_rst_n,
    o_clk_50M,
    o_clk_100M,
    o_clk_100M_shift
);
    input   wire    i_clk_50M;              //外部输入系统时钟，50M
    input   wire    i_rst_n;                //复位，低有效

    output  wire    o_rst_n;                //稳定后的复位信号
    output  wire    o_clk_50M;              //锁相环输出的50M时钟信号
    output  wire    o_clk_100M;             //输出的二倍频时钟，100M，无相移
    output  wire    o_clk_100M_shift;       //输出的二倍频时钟，100M，相移30°，后续根据需要会再调整

    //连线声明
    wire    locked;                         //pll输出稳定标志信号

    pll U_pll(
	    .areset (~i_rst_n),                 //复位，高有效
	    .inclk0 (i_clk_50M),

	    .c0     (o_clk_50M),                //稳定后的复位信号
	    .c1     (o_clk_100M),               //锁相环输出的50M时钟信号
	    .c2     (o_clk_100M_shift),         //输出的二倍频时钟，100M，无相移
	    .locked (locked)                    //输出的二倍频时钟，100M，相移30°，后续根据需要会再调整
    );

    assign  o_rst_n = i_rst_n & locked;     //稳定后的复位信号

endmodule