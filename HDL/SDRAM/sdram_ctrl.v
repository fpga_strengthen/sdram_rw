//信号数量较少时，按照输入输出划分；信号数量较多时，按照功能划分端口
module sdram_ctrl(    
    //时钟和复位信号
    input   wire        i_clk_100M,
    input   wire        i_rst_n, 

    //SDRAM写端口
    input   wire        i_wr_req,               //FIFO控制模块提供
    input   wire [23:0] iv_wr_addr,
    input   wire [15:0] iv_wr_data,
    input   wire [9:0]  iv_wr_burst_len,
    output  wire        o_wr_ack,               //与写数据同步的ack信号

    //SDRAM读端口
    input   wire        i_rd_req,               //FIFO控制模块提供
    input   wire [23:0] iv_rd_addr,
    input   wire [9:0]  iv_rd_burst_len,
    output  wire [15:0] ov_rd_data_out,         //读FIFO读出的数据
    output  wire        o_rd_ack,

    //SDRAM硬件接口
    output  wire        o_sdram_clk_cke,
    output  wire        o_sdram_cs_n,
    output  wire        o_sdram_ras_n,
    output  wire        o_sdram_cas_n,
    output  wire        o_sdram_we_n,
    output  wire [1:0]  ov_sdram_bank,
    output  wire [12:0] ov_sdram_addr,

    //初始化完成信号
    output  wire        o_init_end,

    //接收和发送数据的端口
    inout   wire [15:0] io_sdram_dq             //与顶层的数据输入输出端口连接
);

//-----------------------模块连线----------------------//
    
    //initial
    wire    [3:0]   init_cmd ;
    wire    [1:0]   init_bank;
    wire    [12:0]  init_addr;

    //auto refresh
    wire           aref_req ;   
    wire    [3:0]  aref_cmd ;  
    wire           aref_end ;   
    wire    [12:0] aref_addr; 
    wire    [1:0]  aref_bank; 

    //write
    wire           wr_end;              
    wire           wr_sdram_en;   
    wire    [3:0]  wr_cmd;       
    wire    [1:0]  wr_bank;      
    wire    [12:0] wr_addr;      
    wire    [15:0] wr_sdram_data;

    //read
    wire           rd_end;
    wire    [3:0]  rd_cmd;
    wire    [1:0]  rd_bank;
    wire    [12:0] rd_addr;

    //arbit
    wire            sdram_aref_en;          
    wire            sdram_rd_en;
    wire            sdram_wr_en; 

//----------------------------模块实例化-----------------------------//

//SDRAM初始化
    sdram_init U_sdram_init(
        .i_clk_100M     (i_clk_100M),
        .i_rst_n        (i_rst_n),

        .ov_init_cmd    (init_cmd ),
        .ov_init_bank   (init_bank),
        .ov_init_addr   (init_addr),
        .o_init_end     (o_init_end)
    );

//SDRAM自动刷新
    sdram_aref U_sdram_aref(
        .i_clk_100M     (i_clk_100M),
        .i_rst_n        (i_rst_n),
        .i_init_end     (o_init_end),
        .i_aref_en      (sdram_aref_en),

        .ov_aref_cmd    (aref_cmd ),
        .ov_aref_bank   (aref_bank ),
        .ov_aref_addr   (aref_addr ),
        .o_aref_end     (aref_end),
        .o_aref_req     (aref_req)
    );

//sdram写操作
    sdram_write U_sdram_write(
        .i_clk_100M      (i_clk_100M),
        .i_rst_n         (i_rst_n),
        .i_init_end      (o_init_end),
        .iv_wr_addr      (iv_wr_addr),              //来自FIFO控制模块，提供数据、地址和突发长度
        .iv_wr_data      (iv_wr_data),
        .iv_wr_burst_len (iv_wr_burst_len),
        .i_wr_en         (sdram_wr_en),

        .o_wr_end        (wr_end),
        .o_wr_sdram_en   (wr_sdram_en),
        .ov_wr_cmd       (wr_cmd),
        .ov_wr_bank      (wr_bank),
        .ov_wr_addr      (wr_addr),
        .o_wr_ack        (o_wr_ack),
        .ov_wr_sdram_data(wr_sdram_data)
    );

//sdram读操作
    sdram_read U_sdram_read(
        .i_clk_100M      (i_clk_100M),
        .i_rst_n         (i_rst_n),
        .i_init_end      (o_init_end),
        .i_rd_en         (sdram_rd_en),
        .iv_rd_addr      (iv_rd_addr),              //来自FIFO控制模块，提供数据、地址和突发长度
        .iv_rd_data      (io_sdram_dq),
        .iv_rd_burst_len (iv_rd_burst_len),

        .o_rd_end        (rd_end),
        .ov_rd_cmd       (rd_cmd),
        .ov_rd_bank      (rd_bank),
        .ov_rd_addr      (rd_addr),
        .o_rd_ack        (o_rd_ack),
        .ov_rd_sdram_data(ov_rd_data_out)           //输出给串口模块
    );

//仲裁
    sdram_arbit U_sdram_arbit(
        .i_clk_100M         (i_clk_100M),
        .i_rst_n            (i_rst_n),

        .i_init_end         (o_init_end),
        .iv_init_cmd        (init_cmd),
        .iv_init_bank       (init_bank),
        .iv_init_addr       (init_addr),

        .i_aref_req         (aref_req),
        .i_aref_end         (aref_end),
        .iv_aref_cmd        (aref_cmd),
        .iv_aref_bank       (aref_bank),
        .iv_aref_addr       (aref_addr),

        .i_wr_req           (i_wr_req),
        .i_wr_end           (wr_end),
        .i_wr_sdram_en      (wr_sdram_en),
        .iv_wr_cmd          (wr_cmd),
        .iv_wr_bank         (wr_bank),
        .iv_wr_addr         (wr_addr),
        .iv_wr_data         (wr_sdram_data),

        .i_rd_req           (i_rd_req),
        .i_rd_end           (rd_end),
        .iv_rd_cmd          (rd_cmd),
        .iv_rd_bank         (rd_bank),
        .iv_rd_addr         (rd_addr),

        .o_clk_cke          (o_sdram_clk_cke),
        .o_sdram_cs_n       (o_sdram_cs_n),
        .o_sdram_ras_n      (o_sdram_ras_n),
        .o_sdram_cas_n      (o_sdram_cas_n),
        .o_sdram_we_n       (o_sdram_we_n),
        .ov_sdram_bank      (ov_sdram_bank),
        .ov_sdram_addr      (ov_sdram_addr),

        .o_aref_en          (sdram_aref_en),
        .o_wr_en            (sdram_wr_en),
        .o_rd_en            (sdram_rd_en),
        //.ov_rd_data         (rd_sdram_data),                //从SDRAM读出的数据，传给读模块

        .io_sdram_dq        (io_sdram_dq)
);

endmodule