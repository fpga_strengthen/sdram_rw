module sdram_init(
    i_clk_100M,
    i_rst_n,
    ov_init_cmd,
    ov_init_bank,
    ov_init_addr,
    o_init_end
);
    input   wire        i_clk_100M;             //系统时钟，100M
    input   wire        i_rst_n;

    output  reg [3:0]   ov_init_cmd;
    output  reg [1:0]   ov_init_bank;
    output  reg [12:0]  ov_init_addr;
    output  reg         o_init_end;

//-------------------------------变量、常量和连线声明--------------------------//

    //控制指令,四个比特依次对应{cs_n,ras_n,cas_n,we_n}
    parameter   CMD_INIBIT = 4'b1xxx,                       //命令禁止，此时片选信号无效
                NOP        = 4'b0111,                       //空操作命令
                PRE_CHARGE = 4'b0010,                       //预充电
                AUTO_REF   = 4'b0001,                       //自动刷新
                LMR        = 4'b0000;                       //配置模式寄存器
    
    //初始地址,对应数据手册P45,配置A[12]~A[0]，其中3’b011对应潜伏期CAL Latency，在数据手册P45
    parameter   INIT_ADDR  = {3'b000,1'b0,2'b00,3'b011,1'b0,3'b111};                         

    //状态机跳转等待时间设定
    parameter   CNT_DELAY_MAX = 15'd20_000,                 //上电后需要等待的时间，至少100us，这里设置200us，提高程序的适用度
                //Precharge命令周期，即写入预充电指令后的等待时间，至少18ns，这里设置50ns，5个时钟周期
                CNT_TRP_MAX   = 3'd5,
                //自动刷新周期，至少60ns，设置70ns，7个时钟周期                   
                CNT_TRFC_MAX  = 3'd7,
                //从写入LMR命令到激活/刷新命令之间的等待时间，为两个tCK，在-6A条件下tCK=6ns,-75和7E条件下tCK=7.5ns，这里设置3个时钟周期30ns
                CNT_TMRD_MAX  = 3'd3;

    //状态变量采用格雷码表示
    parameter   INIT_IDLE = 3'b0_00,                //上电后的空闲等待状态，必须等待至少100us
                INIT_PRE  = 3'b0_01,                //上电等待结束后写入预充电指令
                INIT_TRP  = 3'b0_11,                //预充电指令写入完成后等待
                INIT_AREF = 3'b0_10,                //等待结束后写入自动刷新指令（注意自动刷新至少要执行两次

                INIT_TRFC = 3'b1_10,                //写入完成后等待，用来表示自动刷新指令写入完成后的等待时间
                INIT_LMR  = 3'b1_11,                //两次 自动刷新+等待 完成后写入配置模式寄存器的指令
                INIT_TMRD = 3'b1_01,                //写入完成后等待
                INIT_END  = 3'b1_00;                //等待结束后结束

    parameter   NUM_AREF = 4'd8;                    //要执行自动刷新的次数

    reg     [14:0]  cnt_delay;                      //上电后的等待时间
    wire            wait_end;                       //等待结束时拉高一个时钟周期
    wire            trp_end;                        //三个标志信号，对应几段延时操作的结束
    wire            trfc_end;   
    wire            tmrd_end;   
    reg             cnt_clk_rst;                    //计数器cnt_clk复位标志，高有效，当拉高时意味着计数器清零，组合逻辑赋值
    reg     [3:0]   cnt_clk;                        //使用一个计数器对不同指令后的等待时间计时
    reg     [3:0]   cnt_aref;                       //对自动刷新操作计数
    reg     [2:0]   state;                          //状态机状态变量
//-------------------------------MAIN CODE----------------------------------//

    //上电延时
    always @(posedge i_clk_100M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            cnt_delay <= 15'd0;
        else if(cnt_delay == CNT_DELAY_MAX)
            cnt_delay <= CNT_DELAY_MAX;             //cnt_delay <= cnt_delay;
        else if(state == INIT_IDLE)
            cnt_delay <= cnt_delay + 1'b1;
        else
            cnt_delay <= cnt_delay;
    end

    //上电延时等待结束时拉高一个时钟周期
    assign  wait_end = (cnt_delay == CNT_DELAY_MAX - 1'b1)&&(state == INIT_IDLE);

    //状态机控制
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= INIT_IDLE;
        else begin 
            case(state)
                INIT_IDLE : begin 
                    if(wait_end == 1'b1)
                        state <= INIT_PRE;
                    else
                        state <= state;
                end
                INIT_PRE : state <= INIT_TRP;
                INIT_TRP : begin 
                    if(trp_end == 1'b1)
                        state <= INIT_AREF;
                    else
                        state <= state;
                end
                INIT_AREF : state <= INIT_TRFC;
                INIT_TRFC : begin 
                    if(trfc_end == 1'b1)begin 
                        if(cnt_aref == NUM_AREF)
                            state <= INIT_LMR;          //当刷新次数足够时，再跳转到LMR配置状态
                        else
                            state <= INIT_AREF;         //刷新次数不够时，仍然跳转到自动刷新状态
                    end
                    else
                        state <= state;                 //延时tRFC不足时，保持在该状态，继续延时
                end
                INIT_LMR : state <= INIT_TMRD;
                INIT_TMRD : begin 
                    if(tmrd_end == 1'b1)
                        state <= INIT_END;
                    else
                        state <= state;
                end
                INIT_END : state <= INIT_END;           //初始化结束时，就保持在该状态
                default  : state <= INIT_IDLE;
            endcase
        end
    end

    //控制三个写指令状态后的延时
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk <= 4'd0;
        else if(cnt_clk_rst == 1'b1)
            cnt_clk <= 4'd0;
        else
            cnt_clk <= cnt_clk + 4'd1;
    end

    //控制时钟复位信号
    always@(*)begin 
        case(state)
            INIT_IDLE : cnt_clk_rst = 1'b1;
            INIT_TRP  : cnt_clk_rst = (trp_end == 1'b1)  ? 1'b1 : 1'b0;
            INIT_TRFC : cnt_clk_rst = (trfc_end == 1'b1) ? 1'b1 : 1'b0;
            INIT_TMRD : cnt_clk_rst = (tmrd_end == 1'b1) ? 1'b1 : 1'b0;
            INIT_END  : cnt_clk_rst = 1'b1;
            default   : cnt_clk_rst = 1'b0;             //其他状态下，均不进行时钟复位
        endcase
    end

    //三段延时的结束标志信号
    assign  trp_end  = (cnt_clk == CNT_TRP_MAX  - 1'b1)&&(state == INIT_TRP );
    assign  trfc_end = (cnt_clk == CNT_TRFC_MAX - 1'b1)&&(state == INIT_TRFC);
    assign  tmrd_end = (cnt_clk == CNT_TMRD_MAX - 1'b1)&&(state == INIT_TMRD);

    //对刷新次数计数
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_aref <= 4'd0;
        else if(state == INIT_AREF)
            cnt_aref <= cnt_aref + 4'd1;                //对自动刷新次数计数
        else
            cnt_aref <= cnt_aref;
    end

    //输出的控制命令同时起作用，因此同时进行赋值
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin
            ov_init_cmd  <= NOP;
            ov_init_bank <= 2'b11;
            ov_init_addr <= 13'h1FFF;
        end
        else begin 
            case(state)
                INIT_IDLE,INIT_TRP,INIT_TRFC,INIT_TMRD:begin 
                    ov_init_cmd  <= NOP;
                    ov_init_bank <= 2'b11;
                    ov_init_addr <= 13'h1FFF;
                end
                INIT_PRE : begin 
                    ov_init_cmd  <= PRE_CHARGE;
                    ov_init_bank <= 2'b11;
                    ov_init_addr <= 13'h1FFF;
                end
                INIT_AREF:begin 
                    ov_init_cmd  <= AUTO_REF;
                    ov_init_bank <= 2'b11;
                    ov_init_addr <= 13'h1FFF;
                end
                INIT_LMR : begin 
                    ov_init_cmd  <= LMR;
                    ov_init_bank <= 2'b00;
                    ov_init_addr <= INIT_ADDR;
                end
                default:begin 
                    ov_init_cmd  <= NOP;
                    ov_init_bank <= 2'b11;
                    ov_init_addr <= 13'h1FFF;
                end
            endcase
        end
    end

    //发送初始化完成信号
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_init_end <= 1'b0;
        else if(tmrd_end == 1'b1)
            o_init_end <= 1'b1;
        else
            o_init_end <= o_init_end;
    end


endmodule