module sdram_aref(
    i_clk_100M,
    i_rst_n,
    i_init_end,
    i_aref_en,
    ov_aref_cmd,
    ov_aref_bank,
    ov_aref_addr,
    o_aref_end,
    o_aref_req
);
//---------------------------端口列表----------------------------//
    
    input   wire        i_clk_100M;             //系统时钟，50M
    input   wire        i_rst_n;                //复位，低有效
    input   wire        i_init_end;             //SDRAM初始化完成信号
    input   wire        i_aref_en;              //SDRAM自动刷新使能信号，当该信号有效时才能开始自动刷新操作
    
    output  reg         o_aref_req;             //自动刷新请求信号，有效时会持续拉高，直到该次刷新操作完成
    output  reg  [3:0]  ov_aref_cmd;            //控制命令
    output  wire        o_aref_end;             //每完成一次刷新操作的结束标志
    output  wire [12:0] ov_aref_addr;           //数据地址
    output  wire [1:0]  ov_aref_bank;           //Bank地址

//---------------------参数、连线和寄存器变量-----------------------//

    //状态机状态声明，6个状态，One-hot编码
    parameter   AREF_IDLE = 6'b000_001,         //空闲状态
                AREF_PRE  = 6'b000_010,         //写预充电指令状态
                AREF_TRP  = 6'b000_100,         //等待
                AREF      = 6'b001_000,         //自动刷新指令写入
                AREF_TRFC = 6'b010_000,         //等待
                AREF_END  = 6'b100_000;         //一次刷新操作完成

    //配置指令
    parameter   NOP      = 4'b0111,             //空操作指令，占用端口
                PCH      = 4'b0010,             //预充电命令
                AUTO_REF = 4'b0001;             //自动刷新指令

    //tTRP至少18ns，这里设置30ns，3个时钟周期;tTRFC至少60ns，设置70ns，7个时钟周期 
    parameter   TRP = 3, TRFC = 7;              //指令间等待间隔

    parameter   MAX_CNT_ITV = 10'd750;          //刷新操作间隔的时钟周期数

    reg [9:0]   cnt_aref_itv;                   //每次刷新操作间隔（不是写刷新指令的间隔）计时器;itv指interval
    reg [5:0]   state;                          //状态机状态变量
    reg [3:0]   cnt_clk;                        //指令等待间隔计数器
    wire        aref_ack;                       //在发送刷新请求后收到有效刷新信号拉高该信号，作为后续刷新的起始操作指令
    reg         cnt_clk_rst;                    //时钟复位信号
    
    wire        trp_end;
    wire        trfc_end;

    reg [2:0]   cnt_aref;                       //对自动刷新指令计数

//-------------------------MAIN CODE------------------------------//

    assign  ov_aref_bank = 2'b11;               //Bank地址
    assign  ov_aref_addr = 13'h1FFF;            //数据地址

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_aref_itv <= 10'd0;
        else if(cnt_aref_itv == MAX_CNT_ITV - 1'b1)
            cnt_aref_itv <= 10'd0;
        else if(i_init_end == 1'b1)
            cnt_aref_itv <= cnt_aref_itv + 1'b1;
        else
            cnt_aref_itv <= cnt_aref_itv;
    end

    //刷新请求信号
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_aref_req <= 1'b0;
        else if(cnt_aref_itv == MAX_CNT_ITV - 1'b1)
            o_aref_req <= 1'b1;
        else if(aref_ack == 1'b1)
            o_aref_req <= 1'b0;                 //当收到了应答信号后就不再发送请求
        else
            o_aref_req <= o_aref_req;
    end

    assign  aref_ack = (state == AREF_PRE);

    //状态机状态跳转
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= AREF_IDLE;
        else begin 
            case(state)
                AREF_IDLE : begin 
                    if((i_init_end == 1'b1)&&(i_aref_en == 1'b1))
                        state <= AREF_PRE;      //当收到自动刷新的允许指令时再开始跳转到写指令状态
                    else
                        state <= state;
                end
                AREF_PRE : 
                    state <= AREF_TRP;
                AREF_TRP : begin 
                    if(trp_end == 1'b1)
                        state <= AREF;
                    else
                        state <= state;
                end
                AREF : 
                    state <= AREF_TRFC;
                AREF_TRFC : begin
                    if(trfc_end == 1'b1)begin 
                        if(cnt_aref == 3'd2)
                            state <= AREF_END;
                        else
                            state <= AREF;
                    end                    
                    else
                        state <= state;
                end
                AREF_END :
                    state <= AREF_IDLE;
                default  : state <= AREF_IDLE; 
            endcase
        end
    end
    
    //配置时钟复位信号，组合逻辑
    always@(*)begin 
        case(state)
            AREF_IDLE : cnt_clk_rst = 1'b1;
            AREF_TRP  : cnt_clk_rst = (trp_end == 1'b1 ) ? 1'b1 : 1'b0;
            AREF_TRFC : cnt_clk_rst = (trfc_end == 1'b1) ? 1'b1 : 1'b0;
            AREF_END  : cnt_clk_rst = 1'b1;
            default   : cnt_clk_rst = 1'b0;                 //其他状态均不复位
        endcase
    end

    //配置计数间隔
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk <= 4'd0;
        else if(cnt_clk_rst == 1'b1)
            cnt_clk <= 4'd0;
        else
            cnt_clk <= cnt_clk + 1'b1;
    end

    //配置等待结束信号
    assign  trp_end  = (state == AREF_TRP )&&(cnt_clk == TRP - 1'b1 );
    assign  trfc_end = (state == AREF_TRFC)&&(cnt_clk == TRFC - 1'b1);

    //对刷新指令计数
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_aref <= 3'd0;
        else if(state == AREF)
            cnt_aref <= cnt_aref + 3'd1;
        else if(state == AREF_IDLE)
            cnt_aref <= 3'd0;
        else
            cnt_aref <= cnt_aref;
    end

    //配置输出命令、Bank地址和数据地址
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ov_aref_cmd <= NOP;
        else if(state == AREF_PRE)
            ov_aref_cmd <= PCH;             //预充电指令
        else if(state == AREF)
            ov_aref_cmd <= AUTO_REF;        //自动刷新指令
        else 
            ov_aref_cmd <= NOP;
    end

    //刷新操作完成标志信号
    assign  o_aref_end = (state == AREF_END);

endmodule