module sdram_arbit(
    i_clk_100M,
    i_rst_n,

    i_init_end,
    iv_init_cmd,
    iv_init_bank,
    iv_init_addr,

    i_aref_req,
    i_aref_end,
    iv_aref_cmd,
    iv_aref_bank,
    iv_aref_addr,
    o_aref_en,
    
    i_wr_req,
    i_wr_end,
    i_wr_sdram_en,
    iv_wr_cmd,
    iv_wr_bank,
    iv_wr_addr,
    iv_wr_data,
    o_wr_en,

    i_rd_req,
    i_rd_end,
    iv_rd_cmd,
    iv_rd_bank,
    iv_rd_addr,
    o_rd_en,

    o_clk_cke,
    o_sdram_cs_n,
    o_sdram_ras_n,
    o_sdram_cas_n,
    o_sdram_we_n,
    ov_sdram_bank,
    ov_sdram_addr,
    
    io_sdram_dq
);
//-----------------------------------端口列表声明------------------------------------//
    input   wire        i_clk_100M;                 //时钟，100M
    input   wire        i_rst_n;                    //复位，低有效

    input   wire        i_init_end;                 //初始化完成信号
    input   wire [3:0]  iv_init_cmd;                //初始化控制字
    input   wire [1:0]  iv_init_bank;               //初始化Bank地址
    input   wire [12:0] iv_init_addr;               //初始化数据总线

    input   wire        i_aref_req;                 //自动刷新请求信号
    input   wire        i_aref_end;                 //自动刷新操作结束
    input   wire [3:0]  iv_aref_cmd;                //自动刷新控制字
    input   wire [1:0]  iv_aref_bank;               //自动刷新的Bank地址
    input   wire [12:0] iv_aref_addr;               //自动刷新的总线地址
       
    input   wire        i_wr_req;                   //写请求，来自FIFO控制模块
    input   wire        i_wr_end;                   //写结束
    input   wire        i_wr_sdram_en;              //与写数据同步的使能信号
    input   wire [3:0]  iv_wr_cmd;                  //写操作控制字
    input   wire [1:0]  iv_wr_bank;                 //写Bank地址
    input   wire [12:0] iv_wr_addr;                 //写数据总线地址
    input   wire [15:0] iv_wr_data;                 //写数据，来自FIFO控制模块
 
    input   wire        i_rd_req;                   //读请求
    input   wire        i_rd_end;                   //读结束
    input   wire [3:0]  iv_rd_cmd;                  //读操作控制字
    input   wire [1:0]  iv_rd_bank;                 //读Bank地址
    input   wire [12:0] iv_rd_addr;                 //读数据总线地址

    output  reg         o_aref_en;                  //自动刷新使能
    output  reg         o_wr_en;                    //写使能信号
    output  reg         o_rd_en;                    //读使能信号

    output  wire        o_clk_cke;                  //sdram时钟使能信号，常高
    output  reg         o_sdram_cs_n;               //片选信号
    output  reg         o_sdram_ras_n;              //行选通
    output  reg         o_sdram_cas_n;              //列选通
    output  reg         o_sdram_we_n;               //写使能
    output  reg  [1:0]  ov_sdram_bank;              //Bank地址
    output  reg  [12:0] ov_sdram_addr;              //数据总线地址，对行地址和列地址进行时分复用
    
    inout   wire [15:0] io_sdram_dq;                //与SDRAM交互的数据输入输出

//-----------------------------寄存器、连线和参数列表声明-----------------------------//

    //状态机状态声明，共5个状态，与SDRAM控制模块的操作依次相对应，用独热码表示
    parameter   IDLE  = 5'b0_0001,            //初始状态，对应初始化操作
                ARBIT = 5'b0_0010,            //仲裁状态，控制其他四个操作
                WRITE = 5'b0_0100,            //写状态，对应SDRAM写操作
                READ  = 5'b0_1000,            //读状态，对应SDRAM读操作
                AREF  = 5'b1_0000;            //自动刷新状态，对应SDRAM刷新操作
    
    parameter   NOP   = 4'b0111;            //空操作指令

    reg [4:0]   state;                      //状态机状态变量

    reg [3:0]   sdram_cmd;                  //复用控制字，在不同状态下接收不同的控制字           
    
    //状态机逻辑
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= IDLE;                          //上电时，先对SDRAM初始化
        else begin 
            case(state)
                IDLE : begin 
                    if(i_init_end == 1'b1)
                        state <= ARBIT;
                    else
                        state <= state;
                end
                ARBIT : begin                       //该if语句的前后顺序包含了各操作的优先级：刷新 > 写 > 读
                    if(i_aref_req == 1'b1)
                        state <= AREF;
                    else if(i_wr_req == 1'b1)
                        state <= WRITE;
                    else if(i_rd_req == 1'b1)
                        state <= READ;
                    else
                        state <= state;
                end
                WRITE:begin
                    if(i_wr_end == 1'b1)
                        state <= ARBIT;
                    else
                        state <= state;             //当前操作未结束前，当前操作不会被结束
                end
                READ : begin 
                    if(i_rd_end == 1'b1)
                        state <= ARBIT;
                    else
                        state <= state;
                end
                AREF : begin 
                    if(i_aref_end == 1'b1)
                        state <= ARBIT;
                    else  
                        state <= state;
                end
                default : state <= IDLE;
            endcase
        end
    end

    //配置操作使能信号
    //assign  o_aref_en = (state == AREF)  ? 1'b1 : 1'b0;
    //assign  o_wr_en   = (state == WRITE) ? 1'b1 : 1'b0;
    //assign  o_rd_en   = (state == READ)  ? 1'b1 : 1'b0;

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_aref_en <= 1'b0;
        else if((state == ARBIT)&&(i_aref_req == 1'b1))
            o_aref_en <= 1'b1;
        else if(i_aref_end == 1'b1)
            o_aref_en <= 1'b0;
        else
            o_aref_en <= o_aref_en;
    end

    always @(posedge i_clk_100M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            o_wr_en <= 1'b0;
        else if((state == ARBIT)&&(i_aref_req == 1'b0)&&(i_wr_req == 1'b1))
            o_wr_en <= 1'b1;
        else if(i_wr_end == 1'b1)
            o_wr_en <= 1'b0;
        else
            o_wr_en <= o_wr_en;
    end

    always @(posedge i_clk_100M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            o_rd_en <= 1'b0;
        else if((state == ARBIT)&&(i_aref_req == 1'b0)&&(i_wr_req == 1'b0)&&(i_rd_req == 1'b1))
            o_rd_en <= 1'b1;
        else if(i_rd_end == 1'b1)
            o_rd_en <= 1'b0;
        else 
            o_rd_en <= o_rd_en;
    end


//配置SDRAM地址、数据和命令与教程不同的地方在于这里采用时序逻辑，有一拍的时延
    //配置复用SDRAM控制字
    always@(*)begin 
        case(state)
            IDLE  : sdram_cmd = iv_init_cmd;
            WRITE : sdram_cmd = iv_wr_cmd;
            READ  : sdram_cmd = iv_rd_cmd;
            AREF  : sdram_cmd = iv_aref_cmd;
            ARBIT : sdram_cmd = NOP;
            default:sdram_cmd = NOP;
        endcase
    end

    //配置输出控制字，已无需再对状态额外判断
    always@(*)begin 
        if(i_rst_n == 1'b0)begin 
            o_sdram_cs_n  = 1'b1;
            o_sdram_ras_n = 1'b1;
            o_sdram_cas_n = 1'b1;
            o_sdram_we_n  = 1'b1;
        end
        else begin 
            o_sdram_cs_n  = sdram_cmd[3];
            o_sdram_ras_n = sdram_cmd[2];
            o_sdram_cas_n = sdram_cmd[1];
            o_sdram_we_n  = sdram_cmd[0];
        end
    end

    //配置Bank地址和总线地址
    always@(*)begin 
        //if(i_rst_n == 1'b0)begin
        //    ov_sdram_bank = 2'b11;
        //    ov_sdram_addr = 13'h1FFF;
        //end
        //else begin 
            case(state)
                IDLE : begin 
                    ov_sdram_bank = iv_init_bank;
                    ov_sdram_addr = iv_init_addr;
                end
                WRITE : begin 
                    ov_sdram_bank = iv_wr_bank;
                    ov_sdram_addr = iv_wr_addr;
                end
                READ  : begin 
                    ov_sdram_bank = iv_rd_bank;
                    ov_sdram_addr = iv_rd_addr;
                end
                AREF  : begin 
                    ov_sdram_bank = iv_aref_bank;
                    ov_sdram_addr = iv_aref_addr;
                end
                ARBIT : begin 
                    ov_sdram_bank = 2'b11;
                    ov_sdram_addr = 13'h1FFF;
                end
                default : begin 
                    ov_sdram_bank <= 2'b11;
                    ov_sdram_addr <= 13'h1FFF;
                end
            endcase
        //end
    end

    //配置时钟使能信号clk_cke
    assign  o_clk_cke = 1'b1;               //常高即可

    //配置输入输出sdram_dq
    assign  io_sdram_dq = (i_wr_sdram_en == 1'b1) ? iv_wr_data : 16'hzzzz;      //写状态时输出

endmodule