module sdram_read(
    i_clk_100M,
    i_rst_n,
    i_init_end,
    i_rd_en,
    iv_rd_addr,
    iv_rd_data,
    iv_rd_burst_len,
    o_rd_ack,
    o_rd_end,
    ov_rd_cmd,
    ov_rd_bank,
    ov_rd_addr,
    ov_rd_sdram_data
);
//------------------------端口列表-----------------------------//
    input   wire        i_clk_100M;
    input   wire        i_rst_n;
    input   wire        i_init_end;
    input   wire        i_rd_en;
    input   wire [23:0] iv_rd_addr;
    input   wire [15:0] iv_rd_data;
    input   wire [9:0]  iv_rd_burst_len;

    output  wire        o_rd_ack;
    output  wire        o_rd_end;
    output  reg  [3:0]  ov_rd_cmd;
    output  reg  [1:0]  ov_rd_bank;
    output  reg  [12:0] ov_rd_addr;
    output  wire [15:0] ov_rd_sdram_data;

//-------------------------连线、寄存器和常量声明-------------------------//

    //状态机状态，共9个状态
    parameter   RD_IDLE = 4'b0_000,                 //空闲状态
                RD_ACTV = 4'b0_001,                 //写入激活指令
                RD_TRCD = 4'b0_011,                 //激活后等待
                RD_READ = 4'b0_010,                 //写入读指令
                RD_CASL = 4'b0_110,                 //等待潜伏期
                RD_DATA = 4'b0_111,                 //读数据状态
                RD_PRE  = 4'b0_101,                 //写入预充电指令
                RD_TRP  = 4'b0_100,                 //写入预充电指令后等待
                RD_END  = 4'b1_100;                 //一次读操作完成

    //指令
    parameter   NOP     = 4'b0111,                  //空操作指令 
                ACTIVE  = 4'b0011,                  //激活指令（选择Bank和列，并开始读突发）
                READ    = 4'b0101,                  //读指令
                RD_TERM = 4'b0110,                  //读突发终止
                PCH     = 4'b0010;                  //预充电指令

    //等待时间
    parameter   TRCD = 3,                           //至少18ns(两个周期)，按照3个计算
                TRP  = 3,                           //至少18ns
                CASL = 3;                           //潜伏期，3个clk

    reg [3:0]   state;                              //状态机变量

    wire        trcd_end;                           //tRCD结束
    wire        trp_end;                            //tRP结束
    wire        tcl_end;                            //潜伏期结束
    wire        trd_end;                            //读结束
    wire        burst_term_flag;                    //读突发终止指令的标志

    reg         cnt_clk_rst;                        //时钟计数器复位信号，组合逻辑
    reg [3:0]   cnt_clk;                            //时钟计数器

    //状态机
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= RD_IDLE;
        else begin 
            case(state)
                RD_IDLE : begin 
                    if((i_init_end == 1'b1)&&(i_rd_en == 1'b1))
                        state <= RD_ACTV;
                    else
                        state <= state;
                end
                RD_ACTV : 
                    state <= RD_TRCD;
                RD_TRCD : begin 
                    if(trcd_end == 1'b1)
                        state <= RD_READ;
                    else
                        state <= state;
                end
                RD_READ : 
                    state <= RD_CASL;
                RD_CASL : begin 
                    if(tcl_end == 1'b1)
                        state <= RD_DATA;
                    else
                        state <= state;
                end
                RD_DATA : begin
                    if(trd_end == 1'b1)
                        state <= RD_PRE;
                    else
                        state <= state; 
                end
                RD_PRE  : 
                    state <= RD_TRP; 
                RD_TRP  : begin 
                    if(trp_end == 1'b1)
                        state <= RD_END;
                    else
                        state <= state;
                end
                RD_END  : 
                    state <= RD_IDLE;
                default : 
                    state <= RD_IDLE;
            endcase
        end
    end

    reg [15:0]  rd_data_reg;

    //读入的数据可能发生相位改变，打拍同步
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_data_reg <= 16'd0;
        else
            rd_data_reg <= iv_rd_data;
    end

    //几段状态结束标志信号
    assign  trcd_end = (state == RD_TRCD)&&(cnt_clk == TRCD);
    assign  tcl_end  = (state == RD_CASL)&&(cnt_clk == CASL - 1'b1);
    assign  trd_end  = (state == RD_DATA)&&(cnt_clk == iv_rd_burst_len - 1'b1);          //减1是因为从0开始计数，加上CASL相当于读完后又延时了3个周期   // + CASL
    assign  trp_end  = (state == RD_TRP )&&(cnt_clk == TRP - 1'b1);
    
    //从读指令写入起，在累计时钟周期数等于突发长度时写入突发终止指令
    //突发终止指令写入后，还要再等待等于潜伏周期的时钟周期数之后才会完成一次数据读取
    assign  burst_term_flag = (state == RD_DATA)&&(cnt_clk == (iv_rd_burst_len - 1'b1 - CASL));

    //对需要拉高的状态进行配置，其他均为低
    always@(*)begin 
        case(state)
            RD_IDLE : cnt_clk_rst = 1'b1;
            RD_TRCD : cnt_clk_rst = (trcd_end == 1'b1) ? 1'b1 : 1'b0;
            RD_READ : cnt_clk_rst = 1'b1;
            RD_CASL : cnt_clk_rst = (tcl_end == 1'b1) ? 1'b1 : 1'b0;
            RD_DATA : cnt_clk_rst = (trd_end == 1'b1) ? 1'b1 : 1'b0;
            RD_TRP  : cnt_clk_rst = (trp_end == 1'b1) ? 1'b1 : 1'b0;
            RD_END  : cnt_clk_rst = 1'b1;
            default : cnt_clk_rst = 1'b0;
        endcase
    end

    //非复位状态时就持续计数
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk <= 4'd0;
        else if(cnt_clk_rst == 1'b1)
            cnt_clk <= 4'd0;
        else
            cnt_clk <= cnt_clk + 1'b1;
    end

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            ov_rd_cmd  <= NOP;
            ov_rd_bank <= 2'b11;
            ov_rd_addr <= 13'h1FFF;
        end
        else begin 
            case(state)
                RD_IDLE,RD_TRCD,RD_CASL,RD_TRP : begin 
                    ov_rd_cmd  <= NOP;
                    ov_rd_bank <= 2'b11;
                    ov_rd_addr <= 13'h1FFF;
                end
                RD_ACTV : begin                     //写入激活命令同时写入起始行地址和Bank地址
                    ov_rd_cmd  <= ACTIVE;
                    ov_rd_bank <= iv_rd_addr[23:22];
                    ov_rd_addr <= iv_rd_addr[21:9];                 
                end
                RD_READ : begin                     //写入读命令同时写入起始列地址，Bank地址和之前行操作的仍然相同
                    ov_rd_cmd  <= READ;
                    ov_rd_bank <= iv_rd_addr[23:22];
                    ov_rd_addr <= {4'b0000,iv_rd_addr[8:0]};
                end
                RD_DATA : begin                         //读数据状态时，要在最后一个数据(包含在内)的前等于潜伏期长度的时钟数时写入突发终止命令
                    if(burst_term_flag == 1'b1)begin    //即使写入了突发终止命令，数据也会继续读出，持续的时间等于潜伏周期
                        ov_rd_cmd  <= RD_TERM;
                        ov_rd_bank <= 2'b11;
                        ov_rd_addr <= 13'h1FFF;
                    end
                    else begin 
                        ov_rd_cmd  <= NOP;
                        ov_rd_bank <= 2'b11;
                        ov_rd_addr <= 13'h1FFF;
                    end
                end
                RD_PRE : begin 
                    ov_rd_cmd  <= PCH;
                    ov_rd_bank <= iv_rd_addr[23:22];    //Bank地址；实际应该Don't care
                    ov_rd_addr <= 13'h0400;             //此时A[10]拉高，表示选择所有Bank进行预充电（手册P67）
                end
                default : begin                         //其他状态下命令是NOP，但地址都是dont't care
                    ov_rd_cmd  <= NOP;
                    ov_rd_bank <= 2'b11;
                    ov_rd_addr <= 13'h1FFF;
                end
            endcase
        end
    end

    assign  o_rd_ack = (state == RD_DATA)&&((cnt_clk >= 4'd0)&&(cnt_clk <= iv_rd_burst_len - 1'b1));

    assign  ov_rd_sdram_data = (o_rd_ack == 1'b1) ? rd_data_reg : 16'b0;

    assign  o_rd_end = (state == RD_END);

endmodule