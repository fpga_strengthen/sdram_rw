module sdram_write(
    i_clk_100M,
    i_rst_n,
    i_init_end,
    iv_wr_addr,
    iv_wr_data,
    iv_wr_burst_len,
    i_wr_en,
    o_wr_end,
    o_wr_sdram_en,
    o_wr_ack,
    ov_wr_cmd,
    ov_wr_bank,
    ov_wr_addr,             //该地址会进行时分复用，在不同的时刻传输不同的行/列地址
    ov_wr_sdram_data
);
//---------------------------------端口列表--------------------------------//

    input   wire        i_clk_100M;                     //时钟，100M
    input   wire        i_rst_n;                        //复位，低有效
    input   wire        i_init_end;                     //初始化完成信号
    input   wire [23:0] iv_wr_addr;                     //写地址，其中包括2位宽的Bank地址，9位宽的列地址，13位宽的行地址
    input   wire [15:0] iv_wr_data;                     //写入的数据，位宽16，与SDRAM位宽一致
    input   wire [9:0]  iv_wr_burst_len;                //写突发长度，数值上等于SDRAM一行的存储单元的个数，SDRAM每行有512个存储单元，而且是页突发
    input   wire        i_wr_en;                        //写使能信号

    output  wire        o_wr_end;                       //一次写操作完成信号
    output  wire        o_wr_ack;                       //写应答信号
    output  reg         o_wr_sdram_en;                  //SDRAM写请求信号，请求仲裁模块
    output  reg  [3:0]  ov_wr_cmd;                      //写操作命令
    output  reg  [1:0]  ov_wr_bank;                     //写BANK地址
    output  reg  [12:0] ov_wr_addr;                     //写地址
    output  wire [15:0] ov_wr_sdram_data;               //写入的数据，有高低字节

//--------------------------连线、寄存器和常量声明--------------------------//

    //状态机状态变量，使用格雷码表示
    parameter   WR_IDLE  = 3'b0_00,                     //空闲状态
                WR_ACTV  = 3'b0_01,                     //写入ACTIVE指令状态
                WR_TRCD  = 3'b0_11,                     //指令写入后的等待状态
                WR_WRITE = 3'b0_10,                     //写“写数据指令”状态
                WR_DATA  = 3'b1_10,                     //写数据状态
                WR_PRE   = 3'b1_11,                     //预充电状态
                WR_TRP   = 3'b1_01,                     //等待状态
                WR_END   = 3'b1_00;                     //写操作结束状态，持续一个时钟周期

    //控制命令
    parameter   NOP        = 4'b0111,
                ACTIVE     = 4'b0011,
                WRITE      = 4'b0100,
                BURST_TERM = 4'b0110,
                PCH        = 4'b0010;

    parameter   TRCD = 3,           //至少18ns(两个周期)，按照3个计算
                TRP  = 3;           //至少18ns

    parameter   BANK_DEFAULT = 2'b11;

    reg [2:0]   state;              //状态机状态

    reg [9:0]   cnt_clk;            //等待时间计数器
    reg         cnt_clk_rst;        //等待时间计数器复位信号

    //几段等待时间结束信号
    wire        trcd_end;           //tRCD的等待时间最小18ns，需等待至少2个时钟周期(100M)
    wire        trp_end;            //预充电命令的等待周期
    wire        twr_end;            //写数据结束

    //状态机状态控制
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= WR_IDLE;
        else begin
            case(state)
                WR_IDLE : begin 
                    if((i_wr_en == 1'b1)&&(i_init_end == 1'b1))
                        state <= WR_ACTV;
                    else
                        state <= state;
                end
                WR_ACTV : 
                    state <= WR_TRCD;
                WR_TRCD : begin 
                    if(trcd_end == 1'b1)
                        state <= WR_WRITE;
                    else
                        state <= state;
                end
                WR_WRITE : 
                    state <= WR_DATA;
                WR_DATA : begin
                    if(twr_end == 1'b1)
                        state <= WR_PRE;
                    else    
                        state <= state;
                end
                WR_PRE : 
                    state <= WR_TRP;
                WR_TRP : begin 
                    if(trp_end == 1'b1)
                        state <= WR_END;
                    else
                        state <= state;
                end
                WR_END : 
                    state <= WR_IDLE;
                default : state <= WR_IDLE;
            endcase
        end
    end

    //因为要使得输出的数据、地址和命令同时变化且考虑时序逻辑，因此WR_WRITE状态时也依然拉高时钟复位，晚一拍对cnt_clk计数使能
    always@(*)begin 
        case(state)
            WR_IDLE : cnt_clk_rst = 1'b1;
            WR_TRCD : cnt_clk_rst = (trcd_end == 1'b1) ? 1'b1 : 1'b0;
            WR_WRITE: cnt_clk_rst = 1'b1;
            WR_DATA : cnt_clk_rst = (twr_end == 1'b1) ? 1'b1 : 1'b0;
            WR_TRP  : cnt_clk_rst = (trp_end == 1'b1) ? 1'b1 : 1'b0;
            WR_END  : cnt_clk_rst = 1'b1;
            default : cnt_clk_rst = 1'b0;
        endcase
    end

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0) 
            cnt_clk <= 10'd0;
        else if(cnt_clk_rst == 1'b1)
            cnt_clk <= 10'd0;
        else
            cnt_clk <= cnt_clk + 1'b1;
    end

    assign  trcd_end = (state == WR_TRCD)&&(cnt_clk == TRCD - 1'b1);
    assign  twr_end  = (state == WR_DATA)&&(cnt_clk == iv_wr_burst_len - 1'b1);
    assign  trp_end  = (state == WR_TRP )&&(cnt_clk == TRP - 1'b1);

    //配置wr_ack信号
    //always@(posedge i_clk_100M or negedge i_rst_n)begin 
    //    if(i_rst_n == 1'b0)
    //        o_wr_ack <= 1'b0;
    //    else if(trcd_end == 1'b1)
    //        o_wr_ack <= 1'b1;
    //    else if(twr_end == 1'b1)
    //        o_wr_ack <= 1'b0;
    //    else
    //        o_wr_ack <= o_wr_ack;
    //end
    assign      o_wr_ack = ((state == WR_WRITE)||( (state == WR_DATA)&&(cnt_clk <= iv_wr_burst_len - 2'd2)));

    //配置写命令、Bank地址、写地址和数据
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            ov_wr_cmd  <= NOP;
            ov_wr_bank <= BANK_DEFAULT;
            ov_wr_addr <= 13'h1FFF;
        end
        else if((state == WR_IDLE)||(state == WR_TRCD)||(state == WR_TRP))begin 
            ov_wr_cmd  <= NOP;           
            ov_wr_bank <= BANK_DEFAULT;
            ov_wr_addr <= 13'h1FFF;  
        end
        else if(state == WR_ACTV)begin                  //ACTIVE状态时，写入ACTIVE指令、行地址、Bank地址
            ov_wr_cmd  <= ACTIVE;                       //ACTIVE指令
            ov_wr_bank <= iv_wr_addr[23:22];            //Bank地址
            ov_wr_addr <= iv_wr_addr[21:9];             //写行地址
        end
        else if(state == WR_WRITE)begin                 //写指令状态，写入控制指令和突发列地址
            ov_wr_cmd  <= WRITE;                        //写数据指令
            ov_wr_bank <= iv_wr_addr[23:22];            //此时的bank地址和ACTIVE状态时保持相同
            ov_wr_addr <= {4'b0000,iv_wr_addr[8:0]};    //列地址，为了防止出现错误，需要把ov_wr_addr的高四位置零    //iv_wr_addr[8:0]  这样写可能会出错
        end
        else if((state == WR_DATA))begin
            if(twr_end == 1'b1)begin
                ov_wr_cmd  <= BURST_TERM;               //写数据完成时，写入突发终止指令 
                ov_wr_bank <= BANK_DEFAULT;             //Bank地址don't care
                ov_wr_addr <= 13'h1FFF;                 //数据地址don't care
            end
            else begin 
                ov_wr_cmd  <= NOP;                      //写数据完成时，写入突发终止指令 
                ov_wr_bank <= BANK_DEFAULT;             //Bank地址don't care
                ov_wr_addr <= 13'h1FFF;                 //数据地址don't care
            end
        end
        else if(state == WR_PRE)begin                   //突发终止指令结束后，写入预充电指令，并指定Bank地址
            ov_wr_cmd  <= PCH;                          //写数据指令
            ov_wr_bank <= iv_wr_addr[23:22];            //此时不用写Bank地址
            ov_wr_addr <= 13'h0400;                     //写入预充电指令，对所有Bank充电，将A[10]拉高，其他为0，即13'h0_0100_0000_0000
        end
        else if(state == WR_END)begin 
            ov_wr_cmd  <= NOP;                          //一个完整的写操作流程完成
            ov_wr_bank <= BANK_DEFAULT;                 //Don't care
            ov_wr_addr <= 13'h1FFF;                     //Don't care
        end
        else begin
            ov_wr_cmd  <= NOP;                          //keep
            ov_wr_bank <= BANK_DEFAULT;
            ov_wr_addr <= 13'h1FFF;        
        end
    end

    //SDRAM写使能指令
    //assign      o_wr_sdram_en = (state == WR_DATA);

    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_wr_sdram_en <= 1'b0;
        else
            o_wr_sdram_en <= o_wr_ack;
    end

    //在使能信号有效时同步写入数据
    assign      ov_wr_sdram_data = (o_wr_sdram_en == 1'b1) ? iv_wr_data : 16'd0;

    //写操作完成标志
    assign      o_wr_end = (state == WR_END);

endmodule