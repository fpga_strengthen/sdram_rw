module uart_sdram_top(
    //时钟和复位
    input   wire        i_clk_50M,              //外部时钟，50M
    input   wire        i_rst_n,                //复位，低有效
    
    //串口输入、输出
    input   wire        i_rx,                   //串口串行输入bit
    output  wire        o_tx,                   //并串转换后输出bit
    
    //SDRAM硬件接口输出
    output  wire        o_sdram_clk,            //SDRAM时钟
    output  wire        o_sdram_clk_cke,        //时钟使能
    output  wire        o_sdram_cs_n,           //片选，低有效
    output  wire        o_sdram_ras_n,          //行选通，低有效
    output  wire        o_sdram_cas_n,          //列选通，低有效
    output  wire        o_sdram_we_n,           //写使能。低有效
    output  wire [1:0]  ov_sdram_bank,          //Bank地址
    output  wire [12:0] ov_sdram_addr,          //数据总线地址
    output  wire [1:0]  ov_sdram_dqm,           //掩码

    inout   wire [15:0] io_sdram_dq             //数据输入输出端口
);

//--------------------------wire & Const define-------------------------------//
    
    //parameters define
    parameter   UART_BPS = 'd9600,              //串口波特率
                CLK_FREQ = 'd50_000_000;        //时钟频率，50M
    
    parameter   DATA_NUM = 10'd10;              //写入数据数量

    parameter   CNT_WAIT_MAX = 10'd750;         //等待时间

    //wire define
    //pll
    wire            sys_rst_n;
    wire            sys_clk_50M;
    wire            sys_clk_100M;
    wire            sys_clk_100M_shift;
    //uart_rx
    wire    [7:0]   uart_byte;
    wire            byte_sck;
    //sdram_top
    wire    [15:0]  rd_fifo_data;
    wire    [9:0]   rd_fifo_num;
    //fifo_read
    wire            rd_en;
    wire    [7:0]   tx_data;
    wire            tx_flag;

    //reg define
    reg             read_valid;         //先保持一段时间的低电平，染后再拉高，完成了定义的DATA_NUM个数据的读取之后再拉低
    reg     [15:0]  cnt_low_wait;       //对read_valid保持低电平的时间计时
    reg     [9:0]   data_num;           //对写入SDRAM的数据个数计数

    //read_valid generate

    //对从串口接收的字节数据计数
    always@(posedge sys_clk_50M or negedge sys_rst_n)begin
        if(sys_rst_n == 1'b0)
            data_num <= 10'd0;
        else if(read_valid == 1'b1)
            data_num <= 10'd0;              //每当完成一次突发长度的数据读取，就清空计数器
        else if(byte_sck == 1'b1)
            data_num <= data_num + 1'b1;
        else
            data_num <= data_num;
    end

    //当串口发送的足够一次突发长度时，延时一段时间，之后拉高读有效
    always@(posedge sys_clk_50M or negedge sys_rst_n) begin
        if(sys_rst_n == 1'b0)
            cnt_low_wait <= 16'd0;
        else if(cnt_low_wait == CNT_WAIT_MAX)
            cnt_low_wait <= 16'd0;
        else if(data_num == DATA_NUM)
            cnt_low_wait <= cnt_low_wait + 1'b1;
        else
            cnt_low_wait <= cnt_low_wait;
    end

    //延时足够时开始读；当读完突发长度的数据后拉低
    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            read_valid <= 1'b0;
        else if(cnt_low_wait == CNT_WAIT_MAX)
            read_valid <= 1'b1;
        else if(rd_fifo_num == DATA_NUM)
            read_valid <= 1'b0;
        else
            read_valid <= read_valid;
    end

//------------------------- Module Instantiation--------------------------//

    //锁相环产生稳定时钟和复位
    clk_gen U_clk_gen(
        .i_clk_50M          (i_clk_50M),
        .i_rst_n            (i_rst_n  ),

        .o_rst_n            (sys_rst_n         ),
        .o_clk_50M          (sys_clk_50M       ),
        .o_clk_100M         (sys_clk_100M      ),
        .o_clk_100M_shift   (sys_clk_100M_shift)
    );

    //Uart串口接收串行比特并完成串并转换
    uart_rx #(
        .UART_BPS           (UART_BPS),
        .CLK_FREQ           (CLK_FREQ)
    )
    U_uart_rx(
        .sys_clk            (sys_clk_50M),
        .sys_rst_n          (sys_rst_n  ),
        .rx                 (i_rx       ),

        .ov_uart_byte       (uart_byte  ),
        .o_uart_byte_sck    (byte_sck   )
    );

    sdram_top U_sdram_top(
        //时钟和复位
        .i_clk_100M         (sys_clk_100M      ),              //100M时钟
        .i_rst_n            (sys_rst_n         ),              //复位，低有效
        .i_clk_out          (sys_clk_100M_shift),              //针对相移

        //写FIFO信号
        .i_wr_fifo_wr_clk   (sys_clk_50M     ),                //FIFO写时钟，50M，将串口数据写入FIFO
        .i_wr_fifo_wr_req   (byte_sck        ),                //FIFO写请求，交给仲裁模块以得到ack信号
        .iv_wr_fifo_wr_data ({8'b0,uart_byte}),                //写FIFO写入的数据
        .iv_wr_burst_len    (DATA_NUM        ),                //写突发长度
        .iv_sdram_wr_baddr  (24'b0           ),                //SDRAM写操作的起始地址，baddr~begin_addr
        .iv_sdram_wr_eaddr  ({14'b0,DATA_NUM}),                //SDRAM写操作的结束地址，eaddr~end_addr
        .i_wr_rst           (~sys_rst_n      ),                //写复位信号，对系统复位取反

        //读FIFO信号
        .i_rd_fifo_rd_clk   (sys_clk_50M     ),                //FIFO读时钟，将数据读出后发送给串口
        .i_rd_fifo_rd_req   (rd_en           ),                //FIFO读请求，交给仲裁模块以得到ack信号
        .i_read_valid       (read_valid      ),                //读有效信号
        .iv_rd_burst_len    (DATA_NUM        ),                //读操作的突发长度
        .iv_sdram_rd_baddr  (24'b0           ),                //SDRAM读操作的起始地址，baddr~begin_addr
        .iv_sdram_rd_eaddr  ({14'b0,DATA_NUM}),                //SDRAM读操作的结束地址，eaddr~end_addr
        .i_rd_rst           (~sys_rst_n      ),                //读操作复位信号
    
        //读出数据
        .ov_rd_fifo_data    (rd_fifo_data   ),                 //从FIFO读出数据，时钟50M
        .ov_rd_fifo_num     (rd_fifo_num    ),                 //读FIFO时FIFO中的数据个数

        //SDRMA硬件接口
        .o_sdram_clk        (o_sdram_clk    ),
        .o_sdram_clk_cke    (o_sdram_clk_cke),
        .o_sdram_cs_n       (o_sdram_cs_n   ),
        .o_sdram_ras_n      (o_sdram_ras_n  ),
        .o_sdram_cas_n      (o_sdram_cas_n  ),
        .o_sdram_we_n       (o_sdram_we_n   ),
        .ov_sdram_bank      (ov_sdram_bank  ),
        .ov_sdram_addr      (ov_sdram_addr  ),
        .ov_sdram_dqm       (ov_sdram_dqm   ),                  //掩码
        .io_sdram_dq        (io_sdram_dq    )                   //输入输出端口
    );
    
    //将从SDRAM读出的数据缓存到FIFO中按照串口时序读出给Uart_tx
    fifo2uart U_fifo2uart(
        .i_clk_50M          (sys_clk_50M),
        .i_rst_n            (sys_rst_n  ),
        .iv_rd_fifo_num     (rd_fifo_num),                      //根据数据量来判断是否要进行数据的读取
        .iv_rd_fifo_data    (rd_fifo_data[7:0]),                //读FIFO读取的数据
        .iv_burst_num       (DATA_NUM),                         //突发读取的数量

        .o_rd_en            (rd_en  ),                          //读使能，传给FIFO控制模块
        .ov_tx_data         (tx_data),                          //读出的数据发送到串口
        .o_tx_flag          (tx_flag)                           //与数据同步的标志信号
    );

    //将数据并串转换并发送到串口
    uart_tx #(
        .UART_BPS(UART_BPS),
        .CLK_FREQ(CLK_FREQ)
    )
    U_uart_tx(
        .sys_clk            (sys_clk_50M),
        .sys_rst_n          (sys_rst_n  ),
        .iv_uart_byte       (tx_data    ),
        .i_uart_byte_sck    (tx_flag    ),

        .o_data_bit         (o_tx       )
    );

endmodule