//将SDRAM控制器读出的数据缓存到FIFO中，按照串口时序读出并输出给Uart_tx
module fifo2uart(
    input   wire        i_clk_50M,
    input   wire        i_rst_n,
    input   wire [9:0]  iv_rd_fifo_num,             //根据数据量来判断是否要进行数据的读取
    input   wire [7:0]  iv_rd_fifo_data,            //读FIFO读取的数据
    input   wire [9:0]  iv_burst_num,               //突发读取的数量

    output  reg         o_rd_en,                    //读使能，传给FIFO控制模块
    output  wire [7:0]  ov_tx_data,                 //读出的数据发送到串口
    output  reg         o_tx_flag                   //与数据同步的标志信号
);

    //baud计数器
    parameter   CNT_BAUD_MAX = 13'd5208;
    
    reg             rd_en_reg;              //读使能打拍，与FIFO控制模块读取的数据同步
    wire    [9:0]   data_fifo_num;          //本地FIFO中存储的数据量
    reg             rd_valid;               //读有效信号，在该信号拉高时，cnt_baud才会一直计数
    reg     [9:0]   cnt_read;               //对从本地FIFO读出的数据计数
    reg     [12:0]  cnt_baud;               //波特计数器，对应一个比特的持续时间
    reg             bit_flag;               //在每个比特稳定（取中间时刻）时拉高表示当前比特有效
    reg     [3:0]   cnt_bit;                //对比特计数，每10个比特（1开始位+一个字节8位+1停止位）算发送一个字节
    reg             rd_fifo_en;             //本地FIFO读使能


    //将读使能信号交给fifo_ctrl，读出数据后会比rd_en晚一拍
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_rd_en <= 1'b0;
        else if(iv_rd_fifo_num == iv_burst_num)
            o_rd_en <= 1'b1;
        //else if(iv_rd_fifo_num == 10'd1)
        //    o_rd_en <= 1'b0;                //读FIFO中只有一个数据时，拉低读使能，不再读取
        else if(data_fifo_num== iv_burst_num - 10'd2)
            o_rd_en <= 1'b0;
        else
            o_rd_en <= o_rd_en;
    end
    
    //将rd_en打一拍与读出的数据同步
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_en_reg <= 1'b0;
        else
            rd_en_reg <= o_rd_en;
    end

    //rd_en_reg可以作为写使能将同步的数据写入FIFO
    fifo_read U_fifo_read(
	    .clock   (i_clk_50M),
	    .data    (iv_rd_fifo_data),
	    .rdreq   (rd_fifo_en),
	    .wrreq   (rd_en_reg),

	    .q       (ov_tx_data),
	    .usedw   (data_fifo_num)
    );

    //读操作有效信号，控制比特持续时间计数器
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_valid <= 1'b0;
        else if(data_fifo_num == iv_burst_num)
            rd_valid <= 1'b1;
        else if(cnt_read == iv_burst_num)
            rd_valid <= 1'b0;
        else 
            rd_valid <= rd_valid;
    end

    //串口波特率9600Baud,换算成持续时间需要对50M时钟计数5208个
    //当读有效时，持续进行计数
    always@(posedge i_clk_50M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            cnt_baud <= 13'd0;
        else if(cnt_baud == CNT_BAUD_MAX - 1'b1)
            cnt_baud <= 13'd0;
        //else if(cnt_read == iv_burst_num)
        //    cnt_baud <= 13'd0;
        else if(rd_valid == 1'b1)
            cnt_baud <= cnt_baud + 1'b1;
        else
            cnt_baud <= cnt_baud;
    end
    
    //在每个比特的中间时刻对其进行采样得到标志信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            bit_flag <= 1'b0;
        else if(cnt_baud == (CNT_BAUD_MAX >> 1'b1))
            bit_flag <= 1'b1;
        else
            bit_flag <= 1'b0;
    end

    //每10个bit视为发送一个字节
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_bit <= 4'd0;
        else if((cnt_bit == 4'd9)&&(bit_flag == 1'b1))
            cnt_bit <= 4'd0;
        else if(bit_flag == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end

    //配置FIFO读使能信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_fifo_en <= 1'b0;
        else if((cnt_bit == 4'd9)&&(bit_flag == 1'b1))
            rd_fifo_en <= 1'b1;
        else
            rd_fifo_en <= 1'b0;
    end

    //对从本地FIFO读出的数据计数
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_read <= 10'd0;
        else if(rd_valid == 1'b0)
            cnt_read <= 10'd0;
        else if(rd_fifo_en == 1'b1)
            cnt_read <= cnt_read + 1'b1;
        else
            cnt_read <= cnt_read;
    end

    //将读使能打拍作为与发送给串口数据同步的sck信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_tx_flag <= 1'b0;
        else
            o_tx_flag <= rd_fifo_en;
    end


endmodule