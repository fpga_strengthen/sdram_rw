`timescale 1ns/1ps

module sdram_ctrl_tb();
    
    //reg define
    reg         i_clk_50M;
    reg         i_rst_n;

    reg         wr_req;
    reg [15:0]  wr_data_in;

    reg         rd_req;

    //wire define
    wire        sys_rst_n;
    wire        sys_clk_50M;
    wire        sys_clk_100M;
    wire        sys_clk_100M_shift;

    wire        sdram_clk_cke;
    wire        sdram_cs_n   ;
    wire        sdram_ras_n  ;
    wire        sdram_cas_n  ;
    wire        sdram_we_n   ;
    wire [1:0]  sdram_bank   ;
    wire [12:0] sdram_addr   ;
    wire [15:0] rd_data_out  ;
    wire        wr_ack       ;
    wire        rd_ack       ;
    wire        init_end     ;
    wire [15:0] sdram_dq     ;


    //clk & rst generate
    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;                      //系统时钟，50M

    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;               //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;               //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;      //SDRAM大小

    defparam    U_sdram_ctrl.U_sdram_aref.MAX_CNT_ITV = 10'd39;

    //模拟产生写请求
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_req <= 1'b1;
        else if(wr_data_in == 10'd10)
            wr_req <= 1'b0;
        else
            wr_req <= wr_req;
    end

    //模拟产生写入的数据
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_data_in <= 16'd0;
        else if(wr_data_in == 16'd10)
            wr_data_in <= 16'd0;
        else if(wr_ack == 1'b1)
            wr_data_in <= wr_data_in + 1'b1;
        else
            wr_data_in <= wr_data_in;
    end

    //生成读使能信号
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            rd_req <= 1'b0;
        else if(wr_req == 1'b0)
            rd_req <= 1'b1;
        else
            rd_req <= rd_req;
    end

    //时钟倍频
    clk_gen U_clk_gen(
        .i_clk_50M       (i_clk_50M),
        .i_rst_n         (i_rst_n),

        .o_rst_n         (sys_rst_n),
        .o_clk_50M       (sys_clk_50M),
        .o_clk_100M      (sys_clk_100M),
        .o_clk_100M_shift(sys_clk_100M_shift)
    );

    //sdram
    sdram_ctrl U_sdram_ctrl(
        .i_clk_100M     (sys_clk_100M),
        .i_rst_n        (sys_rst_n),
        .i_rd_req       (rd_req),
        .i_wr_req       (wr_req),
        .iv_wr_addr     (24'h000_000),              //首地址为0（包含Bank地址、行列地址）
        .iv_rd_addr     (24'h000_000),
        .iv_wr_data     (wr_data_in),
        .iv_wr_burst_len(10'd10),
        .iv_rd_burst_len(10'd10),

        .o_sdram_clk_cke(sdram_clk_cke),
        .o_sdram_cs_n   (sdram_cs_n   ),
        .o_sdram_ras_n  (sdram_ras_n  ),
        .o_sdram_cas_n  (sdram_cas_n  ),
        .o_sdram_we_n   (sdram_we_n   ),
        .ov_sdram_bank  (sdram_bank   ),
        .ov_sdram_addr  (sdram_addr   ),
        .ov_rd_data_out (rd_data_out  ),
        .o_wr_ack       (wr_ack       ),
        .o_rd_ack       (rd_ack       ),
        .o_init_end     (init_end     ),

        .io_sdram_dq    (sdram_dq     )
    );

    //仿真模型
    sdram_model_plus U_sdram_model(
        .Dq     (sdram_dq),                                     //空闲，不连接
        .Addr   (sdram_addr), 
        .Ba     (sdram_bank), 
        .Clk    (sys_clk_100M_shift),                           //时钟100M,但是相位是有所偏移的
        .Cke    (sdram_clk_cke), 
        .Cs_n   (sdram_cs_n), 
        .Ras_n  (sdram_ras_n), 
        .Cas_n  (sdram_cas_n), 
        .We_n   (sdram_we_n), 
        .Dqm    (4'b0),
        .Debug  (1'b1)
    );
endmodule