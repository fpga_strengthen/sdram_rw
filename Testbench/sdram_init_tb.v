`timescale 1ns/1ps
module sdram_init_tb();

    reg             i_clk_50M;
    reg             i_rst_n;
    
    wire    [3:0]   init_cmd; 
    wire    [1:0]   init_bank;
    wire    [12:0]  init_addr;
    wire            init_end;

    //模块间连线
    wire    sys_rst_n;
    wire    sys_clk_50M;
    wire    sys_clk_100M;
    wire    sys_clk_100M_shift;

    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;          //系统时钟，50M

    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;               //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;               //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;      //SDRAM大小

//--------------------------状态观察测试-----------------------//
    //将状态机状态引出并以自然语言表示
    reg     [9*8-1:0]  current_state;              //字符ASCII存储表示

    //在Radix中选择“ASCII”，数据在显示时就会变成字符串
    //但是前边会有位长和格式字符，如72'a494e....，这里的a和二进制的b、十六进制的h是同理，因为ASCII码开头是A
    always@(U_sdram_init.state)begin
        case(U_sdram_init.state)
            3'b0_00 : current_state = "INIT_IDLE";
            3'b0_01 : current_state = "INIT_PRE";
            3'b0_11 : current_state = "INIT_TRP";
            3'b0_10 : current_state = "INIT_AREF";
            3'b1_10 : current_state = "INIT_TRFC";
            3'b1_11 : current_state = "INIT_LMR";
            3'b1_01 : current_state = "INIT_TMRD";
            3'b1_00 : current_state = "INIT_END";
            default : current_state = "INIT_IDLE";
        endcase
    end

    //时钟倍频和相移模块
    clk_gen U_clk_gen(
        .i_clk_50M       (i_clk_50M),
        .i_rst_n         (i_rst_n),

        .o_rst_n         (sys_rst_n),
        .o_clk_50M       (sys_clk_50M),
        .o_clk_100M      (sys_clk_100M),
        .o_clk_100M_shift(sys_clk_100M_shift)
    );

    //SDRAM初始化
    sdram_init U_sdram_init(
        .i_clk_100M      (sys_clk_100M),
        .i_rst_n         (sys_rst_n),
     
        .ov_init_cmd     (init_cmd),
        .ov_init_bank    (init_bank),
        .ov_init_addr    (init_addr),
        .o_init_end      (init_end)
    );

    //SDRAM仿真模型
    sdram_model_plus U_sdram_model(
        .Dq     (),                                     //空闲，不连接
        .Addr   (init_addr), 
        .Ba     (init_bank), 
        .Clk    (sys_clk_100M_shift),                   //时钟100M,但是相位是有所偏移的
        .Cke    (1'b1), 
        .Cs_n   (init_cmd[3]), 
        .Ras_n  (init_cmd[2]), 
        .Cas_n  (init_cmd[1]), 
        .We_n   (init_cmd[0]), 
        .Dqm    (2'b00),
        .Debug  (1'b1)
    );



endmodule