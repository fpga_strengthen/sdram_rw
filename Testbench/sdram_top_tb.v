`timescale 1ns/1ps

module sdram_top_tb();

//reg define
    reg         i_clk_50M;
    reg         i_rst_n;

//wire define
//-------clk_gen--------
    wire        sys_rst_n;
    wire        sys_clk_50M;
    wire        sys_clk_100M;
    wire        sys_clk_100M_shift;

//-------sdram_top------

    wire [15:0] ov_rd_fifo_data;
    wire [9:0]  ov_rd_fifo_num;

    //SDRAM硬件控制信号
    wire        sdram_clk;
    wire        sdram_clk_cke;
    wire        sdram_cs_n;
    wire        sdram_ras_n;
    wire        sdram_cas_n;
    wire        sdram_we_n;
    wire [1:0]  sdram_bank;
    wire [12:0] sdram_addr;
    wire [3:0]  sdram_dqm;   
    wire [15:0] sdram_dq;

//--------clk & rst generate-------------//
    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;                      //系统时钟，50M

    //时钟倍频&相移30°
    clk_gen U_clk_gen(
        .i_clk_50M       (i_clk_50M),
        .i_rst_n         (i_rst_n),

        .o_rst_n         (sys_rst_n),
        .o_clk_50M       (sys_clk_50M),
        .o_clk_100M      (sys_clk_100M),
        .o_clk_100M_shift(sys_clk_100M_shift)
    );

//---------串口数据模拟产生--------//

    reg         wr_en;
    reg         wr_data_flag;
    reg  [15:0] wr_data_in;
    reg  [2:0]  cnt_wr_wait;        //数据周期性产生

//---------------写FIFO------------------//
    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            cnt_wr_wait <= 3'd0;
        else if(wr_en == 1'b1)
            cnt_wr_wait <= cnt_wr_wait + 3'd1;
        else
            cnt_wr_wait <= 3'd0;
    end

    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_data_flag <= 1'b0;
        else if(cnt_wr_wait == 3'd7)
            wr_data_flag <= 1'b1;
        else
            wr_data_flag <= 1'b0;
    end

    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_data_in <= 16'd0;
        else if(cnt_wr_wait == 3'd7)
            wr_data_in <= wr_data_in + 1'b1;
        else
            wr_data_in <= wr_data_in;
    end

    //仿真一开始就写入数据到FIFO，虽然初始化未完成，但数据会一直在FIFO中
    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_en <= 1'b1;
        else if(wr_data_in == 16'd10)
            wr_en <= 1'b0;
        else
            wr_en <= wr_en;
    end

//-----------------读FIFO-----------------//

    reg         rd_en;
    reg [3:0]   cnt_rd_data;
    reg         read_valid;

    wire [15:0] rd_fifo_data;
    wire [9:0]  rd_fifo_num;

    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            rd_en <= 1'b0;
        else if(cnt_rd_data == 4'd9)
            rd_en <= 1'b0;              //计数到9时（0~9，10个数）表示已经读取完毕，读使能拉低
        else if((wr_en == 1'b0)&&(rd_fifo_num == 10'd10))
            rd_en <= 1'b1;              //读FIFO中的数据足够一次突发长度时才能开始读取，拉高读使能信号
        else                            //因此要将rd_fifo_num连接到读FIFO的wr_usedw端口，表示已经写入到FIFO的数据量
            rd_en <= rd_en;
    end

    //对读FIFO读出的数据计数
    always@(posedge sys_clk_50M or negedge sys_rst_n)begin
        if(sys_rst_n == 1'b0)
            cnt_rd_data <= 4'd0;
        else if(rd_en == 1'b1)
            cnt_rd_data <= cnt_rd_data + 1'b1;
        else
            cnt_rd_data <= 4'd0;
    end

    always@(posedge sys_clk_50M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            read_valid <= 1'b1;             //复位使其有效
        else if(rd_fifo_num == 10'd10)
            read_valid <= 1'b0;             //当读FIFO完成时就将其拉低，作用已经发挥完毕
        else
            read_valid <= read_valid;
    end

//--------仿真模型参数重定义---------//
    
    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;               //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;               //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;      //SDRAM大小

    defparam    U_sdram_top.U_sdram_ctrl.U_sdram_aref.MAX_CNT_ITV = 10'd40;

//--------sdram_top inst-----------//
    
    sdram_top U_sdram_top(
        //时钟和复位
        .i_clk_100M (sys_clk_100M),                     //100M时钟
        .i_rst_n    (sys_rst_n),                        //复位，低有效
        .i_clk_out  (sys_clk_100M_shift),               //针对相移

        //写FIFO信号
        .i_wr_fifo_wr_clk   (sys_clk_50M),              //FIFO写时钟，50M，将串口数据写入FIFO
        .i_wr_fifo_wr_req   (wr_data_flag),             //FIFO写请求，交给仲裁模块以得到ack信号
        .iv_wr_fifo_wr_data (wr_data_in),               //写FIFO写入的数据
        .iv_wr_burst_len    (10'd10),                   //写突发长度
        .iv_sdram_wr_baddr  (24'd0),                    //SDRAM写操作的起始地址，baddr~begin_addr
        .iv_sdram_wr_eaddr  (24'd10),                   //SDRAM写操作的结束地址，eaddr~end_addr
        .i_wr_rst           (~sys_rst_n),               //写复位信号，对系统复位取反

        //读FIFO信号
        .i_rd_fifo_rd_clk   (sys_clk_50M),              //FIFO读时钟，将数据读出后发送给串口
        .i_rd_fifo_rd_req   (rd_en),                    //FIFO读请求，交给仲裁模块以得到ack信号
        .i_read_valid       (read_valid),               //读有效信号
        .iv_rd_burst_len    (10'd10),                   //读操作的突发长度
        .iv_sdram_rd_baddr  (24'd0),                    //SDRAM读操作的起始地址，baddr~begin_addr
        .iv_sdram_rd_eaddr  (24'd10),                   //SDRAM读操作的结束地址，eaddr~end_addr
        .i_rd_rst           (~sys_rst_n),               //读操作复位信号
    
        .ov_rd_fifo_data    (rd_fifo_data),             //从FIFO读出数据，时钟50M，发送给串口
        .ov_rd_fifo_num     (rd_fifo_num ),             //FIFO中可供读取的数据个数

        .o_sdram_clk        (sdram_clk    ),
        .o_sdram_clk_cke    (sdram_clk_cke),
        .o_sdram_cs_n       (sdram_cs_n   ),
        .o_sdram_ras_n      (sdram_ras_n  ),
        .o_sdram_cas_n      (sdram_cas_n  ),
        .o_sdram_we_n       (sdram_we_n   ),
        .ov_sdram_bank      (sdram_bank   ),
        .ov_sdram_addr      (sdram_addr   ),
        .ov_sdram_dqm       (sdram_dqm    ),            //掩码
        .io_sdram_dq        (sdram_dq     )             //输入输出端口
);

//----------SDRAM仿真模型------------//
    sdram_model_plus U_sdram_model(
        .Dq     (sdram_dq),                           //双向端口，既是输入又是输出

        .Addr   (sdram_addr), 
        .Ba     (sdram_bank), 
        .Clk    (sdram_clk),                            //时钟100M,但是相位是有所偏移的
        .Cke    (sdram_clk_cke), 
        .Cs_n   (sdram_cs_n), 
        .Ras_n  (sdram_ras_n), 
        .Cas_n  (sdram_cas_n), 
        .We_n   (sdram_we_n), 
        .Dqm    (sdram_dqm),
        .Debug  (1'b1)
    );




endmodule