`timescale 1ns/1ps
module  uart_sdram_top_tb();

//----------------------------reg define--------------------------//
    
    reg         i_clk_50M;
    reg         i_rst_n;
    reg         rx;
    reg [7:0]   data_mem[9:0];      //存储器，存储数据为10个字节

    initial begin
        //读取本地数据并存储
        $readmemh("data.txt",data_mem);
    end

//----------------------------wire define-------------------------//

    wire        tx;
    wire        sdram_clk    ;
    wire        sdram_clk_cke;
    wire        sdram_cs_n   ;  
    wire        sdram_ras_n  ; 
    wire        sdram_cas_n  ; 
    wire        sdram_we_n   ;  
    wire [1:0]  sdram_bank   ; 
    wire [12:0] sdram_addr   ; 
    wire [3:0]  sdram_dqm    ;  
    wire [15:0] sdram_dq     ;

//---------------------   clk & rst generate  ------------------//
    
    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;                      //系统时钟，50M     

//------------------------- rx generate  ---------------------//

    //模拟发送一帧数据
    task rx_bit(
        input   [7:0]   data
    );
        integer i;
        for(i=0;i<10;i=i+1)begin        //发送10bit，包含起始位和停止位，字节从LSB开始发送
            case(i)
                0 : rx <= 1'b0;
                1 : rx <= data[0];
                2 : rx <= data[1];
                3 : rx <= data[2];
                4 : rx <= data[3];
                5 : rx <= data[4];
                6 : rx <= data[5];
                7 : rx <= data[6];
                8 : rx <= data[7];
                9 : rx <= 1'b1;
                default : rx <= 1'b1;
            endcase
            #(52 * 20);                 //比特之间要间隔52个周期
            //（本来5208，但时钟缩小了100倍，这里需要同比缩小，但是波特率仍然保持9600）
        end
    endtask

    //发送读取的字节数据
    task    rx_byte();
        integer j;
        for(j=0;j<10;j=j+1)
            rx_bit(data_mem[j]);
    endtask

    initial begin 
        //执行字节发送任务
        #100
        rx_byte();
    end

//-------------------Model Parameters Redefine--------------------//
    
    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;                   //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;                   //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                    //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;          //SDRAM大小

    defparam    U_uart_sdram_top.CLK_FREQ = 'd500_000;                      //将时钟缩小100倍
    defparam    U_uart_sdram_top.U_fifo2uart.CNT_BAUD_MAX = 13'd52;         //与系统时钟同步缩小计数
    //defparam    U_uart_sdram_top.U_sdram_top.U_sdram_ctrl.U_sdram_aref.MAX_CNT_ITV = 10'd40;        //缩短自动刷新周期

//-----------------------Module Instantiation--------------------//

    //----------------uart_sdram----------------//
    uart_sdram_top U_uart_sdram_top(
        //时钟和复位
        .i_clk_50M(i_clk_50M),                      //外部时钟，50M
        .i_rst_n  (i_rst_n),                        //复位，低有效
        //串口输入、输出
        .i_rx     (rx),                             //串口串行输入bit
        .o_tx     (tx),                             //并串转换后输出bit
        //SDRAM硬件接口输出
        .o_sdram_clk     (sdram_clk    ),           //SDRAM时钟
        .o_sdram_clk_cke (sdram_clk_cke),           //时钟使能
        .o_sdram_cs_n    (sdram_cs_n   ),           //片选，低有效
        .o_sdram_ras_n   (sdram_ras_n  ),           //行选通，低有效
        .o_sdram_cas_n   (sdram_cas_n  ),           //列选通，低有效
        .o_sdram_we_n    (sdram_we_n   ),           //写使能。低有效
        .ov_sdram_bank   (sdram_bank   ),           //Bank地址
        .ov_sdram_addr   (sdram_addr   ),           //数据总线地址
        .ov_sdram_dqm    (sdram_dqm    ),           //掩码
        .io_sdram_dq     (sdram_dq     )            //数据输入输出端口
    );

    //-----------------SDRAM仿真模型-------------//
    sdram_model_plus U_sdram_model(
        .Dq     (sdram_dq     ),                           //双向端口，既是输入又是输出

        .Addr   (sdram_addr   ), 
        .Ba     (sdram_bank   ), 
        .Clk    (sdram_clk    ),                          //时钟100M,但是相位是有所偏移的
        .Cke    (sdram_clk_cke), 
        .Cs_n   (sdram_cs_n   ), 
        .Ras_n  (sdram_ras_n  ), 
        .Cas_n  (sdram_cas_n  ), 
        .We_n   (sdram_we_n   ), 
        .Dqm    (sdram_dqm    ),
        .Debug  (1'b1         )
    );


endmodule