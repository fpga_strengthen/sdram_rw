`timescale 1ns/1ps
//SDRAM写入验证
module sdram_read_tb();

    //clk_gen
    wire            sys_rst_n;
    wire            sys_clk_50M;
    wire            sys_clk_100M;
    wire            sys_clk_100M_shift;

    //sdram_init
    wire    [3:0]   init_cmd; 
    wire    [1:0]   init_bank;
    wire    [12:0]  init_addr;
    wire            init_end;

    //sdram_addr
    wire    [3:0]   sdram_cmd ;             //控制SDRAM的命令，根据初始化/读/写状态来决定命令内容
    wire    [1:0]   sdram_bank;             //写入SDRAM的Bank地址，根据初始化/读/写状态来决定
    wire    [12:0]  sdram_addr;             //写入SDRAM的数据总线地址，根据初始化/读/写状态来决定
    wire    [15:0]  sdram_data;             //写入SDRAM的数据，根据读/写状态来决定是写入的数据还是SDRAM回读的数据

    //sdram_write
    wire            wr_end;                 //SDRAM写操作结束标志信号
    wire            wr_sdram_en;            //SDRAM写使能，当该信号有效时将数据写入SDRAM
    wire            wr_ack;
    wire    [3:0]   wr_cmd;                 //写控制命令
    wire    [1:0]   wr_bank;                //写操作的写入Bank地址
    wire    [12:0]  wr_addr;                //写操作的写数据总线地址
    wire    [15:0]  wr_sdram_data;          //要写入SDRAM的数据，在tb中由write_in产生

    //sdram_read
    wire            rd_ack;
    wire            rd_end;                 //读操作结束信号
    wire    [3:0]   rd_cmd;                 //读操作的命令
    wire    [1:0]   rd_bank;                //读操作的Bank地址
    wire    [12:0]  rd_addr;                //读取的数据总线地址
    wire    [15:0]  rd_sdram_data;          //从SDRAM中读取的数据传出

    //rw_ctrl
    wire    [3:0]   rw_cmd;                 //读/写操作命令，由当前状态是读还是写来决定
    wire    [1:0]   rw_bank;                //读/写Bank地址，由当前状态是读还是写来决定
    wire    [12:0]  rw_addr;                //读/写数据总线地址，由当前状态是读还是写来决定

    //reg define
    reg             i_clk_50M;
    reg             i_rst_n;
    reg             wr_en;
    reg     [15:0]  wr_data_in; 
    reg             rd_en;                  //SDRAM读使能

    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;          //系统时钟，50M

    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;               //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;               //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;      //SDRAM大小

    //时钟倍频和相移模块
    clk_gen U_clk_gen(
        .i_clk_50M       (i_clk_50M),
        .i_rst_n         (i_rst_n),

        .o_rst_n         (sys_rst_n),
        .o_clk_50M       (sys_clk_50M),
        .o_clk_100M      (sys_clk_100M),
        .o_clk_100M_shift(sys_clk_100M_shift)
    );

    //SDRAM初始化
    sdram_init_fb U_sdram_init(
        .i_clk_100M      (sys_clk_100M),
        .i_rst_n         (sys_rst_n),
     
        .ov_init_cmd     (init_cmd),
        .ov_init_bank    (init_bank),
        .ov_init_addr    (init_addr),
        .o_init_end      (init_end)
    );

    //由于init_end暂时处于持续拉高状态，所以wr_en也会周期性拉高，从而会不断的有写操作
    //仿真时一直只有写操作而没有读操作，是因为模拟的写使能配置不对
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_en <= 1'b1;
        else if(wr_end == 1'b1)
            wr_en <= 1'b0;                  //写完时将写使能信号拉低清零
        //else if(init_end == 1'b1)
        //    wr_en <= 1'b1;                //暂时认为初始化完成时，写使能有效；写入完成后拉低
        else
            wr_en <= wr_en;
    end

    //模拟产生要写入的数据
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            wr_data_in <= 16'd0;
        else if(wr_data_in == 16'd52)
            wr_data_in <= 16'h0;
        else if(wr_ack == 1'b1)
            wr_data_in <= wr_data_in + 1'b1;
        else
            wr_data_in <= wr_data_in;
    end

    //不同状态下sdram有不同的控制指令、地址
    assign  sdram_cmd  = (init_end == 1'b1) ? rw_cmd  : init_cmd;
    assign  sdram_bank = (init_end == 1'b1) ? rw_bank : init_bank;
    assign  sdram_addr = (init_end == 1'b1) ? rw_addr : init_addr;

    //这里根据写使能是否有效将读写操作的命令和地址区分开
    assign  rw_cmd  = (wr_en == 1'b1) ? wr_cmd  : rd_cmd;
    assign  rw_bank = (wr_en == 1'b1) ? wr_bank : rd_bank;
    assign  rw_addr = (wr_en == 1'b1) ? wr_addr : rd_addr; 

    //wr_SDRAM_data
    assign  sdram_data = (wr_sdram_en == 1'b1) ? wr_sdram_data : 16'hzzzz;       //此时的sdram_data是output属性

    sdram_write U_sdram_write(
        .i_clk_100M      (sys_clk_100M),
        .i_rst_n         (sys_rst_n),
        .i_init_end      (init_end),
        .iv_wr_addr      (24'h0),                //初始地址{bank[23:22],row_addr[21:9],column_addr[8:0]}全设为0
        .iv_wr_data      (wr_data_in),
        .iv_wr_burst_len (10'd50),               //列地址数量，10bit
        .i_wr_en         (wr_en),                //暂时用初始化完成信号作为写使能信号
 
        .o_wr_end        (wr_end),
        .o_wr_sdram_en   (wr_sdram_en),
        .o_wr_ack        (wr_ack),
        .ov_wr_cmd       (wr_cmd),
        .ov_wr_bank      (wr_bank),
        .ov_wr_addr      (wr_addr),
        .ov_wr_sdram_data(wr_sdram_data)
    );

    //SDRAM读操作部分

    //生成读使能信号
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            rd_en <= 1'b0;
        else if(rd_end == 1'b1)
            rd_en <= 1'b0;
        else if(wr_en == 1'b0)
            rd_en <= 1'b1;
        else
            rd_en <= rd_en;
    end

    //读模块实例化
    sdram_read U_sdram_read(
        .i_clk_100M         (sys_clk_100M),
        .i_rst_n            (sys_rst_n),
        .i_init_end         (init_end),
        .i_rd_en            (rd_en),
        .iv_rd_addr         (24'h000_000),
        .iv_rd_data         (sdram_data),               //由SDRAM返回读出的数据
        .iv_rd_burst_len    (10'd10),

        .o_rd_ack           (rd_ack),
        .o_rd_end           (rd_end),
        .ov_rd_cmd          (rd_cmd),
        .ov_rd_bank         (rd_bank),
        .ov_rd_addr         (rd_addr),
        .ov_rd_sdram_data   (rd_sdram_data)
    );

    //SDRAM仿真模型
    sdram_model_plus U_sdram_model(
        .Dq     (sdram_data),                           //双向端口，既是输入又是输出

        .Addr   (sdram_addr), 
        .Ba     (sdram_bank), 
        .Clk    (sys_clk_100M_shift),                   //时钟100M,但是相位是有所偏移的
        .Cke    (1'b1), 
        .Cs_n   (sdram_cmd[3]), 
        .Ras_n  (sdram_cmd[2]), 
        .Cas_n  (sdram_cmd[1]), 
        .We_n   (sdram_cmd[0]), 
        .Dqm    (2'b00),
        .Debug  (1'b1)
    );

endmodule