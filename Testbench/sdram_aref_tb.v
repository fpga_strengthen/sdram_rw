`timescale 1ns/1ps
module sdram_aref_tb();

    reg             i_clk_50M;
    reg             i_rst_n;

    reg             aref_en;
    
    wire    [3:0]   init_cmd; 
    wire    [1:0]   init_bank;
    wire    [12:0]  init_addr;
    wire            init_end;

    wire    [3:0]   aref_cmd ;  
    wire    [1:0]   aref_bank; 
    wire    [12:0]  aref_addr; 
    wire            aref_end ;  
    wire            aref_req ;  

    wire    [3:0]   sdram_cmd ;
    wire    [1:0]   sdram_bank;
    wire    [12:0]  sdram_addr;

    //模块间连线
    wire    sys_rst_n;
    wire    sys_clk_50M;
    wire    sys_clk_100M;
    wire    sys_clk_100M_shift;

    initial begin
        i_clk_50M = 1'b1;
        i_rst_n <= 1'b0;
        #40
        i_rst_n <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;          //系统时钟，50M

    //根据当前SDRAM的参数重定义仿真模型参数
    defparam    U_sdram_model.addr_bits = 13;               //地址13位，A[12]~A[0]
    defparam    U_sdram_model.data_bits = 16;               //数据16位，有高、低字节
    defparam    U_sdram_model.col_bits  = 9;                //列宽是9位，2^9-1
    defparam    U_sdram_model.mem_sizes = 2*1024*1024;      //SDRAM大小

    //时钟倍频和相移模块
    clk_gen U_clk_gen(
        .i_clk_50M       (i_clk_50M),
        .i_rst_n         (i_rst_n),

        .o_rst_n         (sys_rst_n),
        .o_clk_50M       (sys_clk_50M),
        .o_clk_100M      (sys_clk_100M),
        .o_clk_100M_shift(sys_clk_100M_shift)
    );

    //SDRAM初始化
    sdram_init_fb U_sdram_init(
        .i_clk_100M      (sys_clk_100M),
        .i_rst_n         (sys_rst_n),
     
        .ov_init_cmd     (init_cmd),
        .ov_init_bank    (init_bank),
        .ov_init_addr    (init_addr),
        .o_init_end      (init_end)
    );

    //不同状态下sdram有不同的控制指令、地址
    assign  sdram_cmd  = (init_end == 1'b1) ? aref_cmd  : init_cmd;
    assign  sdram_bank = (init_end == 1'b1) ? aref_bank : init_bank;
    assign  sdram_addr = (init_end == 1'b1) ? aref_addr : init_addr;

    //sdram刷新初始化
    always@(posedge sys_clk_100M or negedge sys_rst_n)begin 
        if(sys_rst_n == 1'b0)
            aref_en <= 1'b0;
        else if(aref_req == 1'b1)                   //todo : (aref_req == 1'b1)&&(i_init_end == 1'b1)
            aref_en <= 1'b1;
        else if(aref_end == 1'b1)
            aref_en <= 1'b0;
        else
            aref_en <= aref_en;
    end

    sdram_aref U_sdram_aref(
        .i_clk_100M     (sys_clk_100M),
        .i_rst_n        (sys_rst_n),
        .i_init_end     (init_end),
        .i_aref_en      (aref_en),

        .ov_aref_cmd    (aref_cmd ),
        .ov_aref_bank   (aref_bank),
        .ov_aref_addr   (aref_addr),
        .o_aref_end     (aref_end ),
        .o_aref_req     (aref_req )
    );

    //SDRAM仿真模型
    sdram_model_plus U_sdram_model(
        .Dq     (),                                     //空闲，不连接
        .Addr   (sdram_addr), 
        .Ba     (sdram_bank), 
        .Clk    (sys_clk_100M_shift),                   //时钟100M,但是相位是有所偏移的
        .Cke    (1'b1), 
        .Cs_n   (sdram_cmd[3]), 
        .Ras_n  (sdram_cmd[2]), 
        .Cas_n  (sdram_cmd[1]), 
        .We_n   (sdram_cmd[0]), 
        .Dqm    (2'b00),
        .Debug  (1'b1)
    );

endmodule